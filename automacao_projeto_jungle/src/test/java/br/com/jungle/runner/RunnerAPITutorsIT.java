package br.com.jungle.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
strict = true
,features = {"features"}
,format = {"json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty"}
,snippets = SnippetType.CAMELCASE
,glue = "br.com.jungle.stepdefinitions"
//,plugin = "br.com.jungle.report.ExtentCucumberFormatter:"
//,tags = {"@RegisterHuman, @Exists, @Register, @CheckEmail, @RegisterDiet, @RegisterPet, @UploadPetFile, @Complete"}
,tags = {"@Complete"}
)

public class RunnerAPITutorsIT {

	
}
