package br.com.jungle.runner;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import br.com.jungle.capabilities.AndroidCapabilities;
import br.com.jungle.capabilities.ChromeCapabilities;
import br.com.jungle.core.DriverFactory;
import br.com.jungle.core.Properties;
import br.com.jungle.core.Properties.ExecutionType;
import br.com.jungle.core.Properties.SystemConfig;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
strict = true
,features = {"features/"}
,format = {"json:target/cucumber-parallel/1.json", "html:target/cucumber-parallel/1.html", "pretty"}
,snippets = SnippetType.CAMELCASE
,glue = "br.com.jungle.stepdefinitions"
,monochrome = false
//,plugin = "br.com.jungle.report.ExtentCucumberFormatter:"
,tags = {"@PetPage"}
)

public class RunnerPetPageIT {

	@BeforeClass
	public static void start() throws IOException, InterruptedException {
				
		System.out.println("Inicializando server...");
		
		/**
		 * Setar as propriedades com ExecutionType: LOCAL_OR SAUCELABS
		 * Setar as propriedades com SystemConfig: CHROME OR CHROMEHEADLESS OR FIREFOX OR ANDROID OR IOS
		 * Setar as propriedades com Capabilities: AndroidCapabilities OR IOSCapabilities OR ChromeCapabilities
		 */
		Properties.setProperties(ExecutionType.LOCAL, SystemConfig.CHROME, ChromeCapabilities.capabilitiesLocal());
		
		System.out.println("Server inicializado...");
	}
	
	@AfterClass
	public static void finish() throws IOException {
		
		System.out.println("Fechando server....");
		//DriverFactory.quitDriver();

	}
}
