package br.com.jungle.stepdefinitions;

import java.util.List;

import org.junit.Assert;

import br.com.jungle.constants.UrlProperties;
import br.com.jungle.dao.Breed;
import br.com.jungle.dao.HealthProblemsDetails;
import br.com.jungle.dao.Pet;
import br.com.jungle.pageobjects.HumanPageObject;
import br.com.jungle.pageobjects.PetPageObject;
import br.com.jungle.pageobjects.PreEmailPageObject;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class PetSteps {
	
	private PreEmailPageObject preEmailPage = new PreEmailPageObject();
	private HumanPageObject humanPage = new HumanPageObject();
	private PetPageObject petPage = new PetPageObject();
	private Pet pet;
	private Breed breed;
	private HealthProblemsDetails healthProblemsDetails;
	
	int valorQuiloEmGramas = 0;
	int quiloEmGramas = 0;
	int valorQuiloIdealEmGramas = 0;

	
	@Dado("^que o usuário tenha cadastrado um pet na pagina de humano \"([^\"]*)\"$")
	public void que_o_usuário_tenha_cadastrado_um_pet_na_pagina_de_humano(String email) throws Throwable {
		
		preEmailPage.navigateTo(UrlProperties.AWS_DEV_WEB);
		//Assert.assertTrue(preEmailPage.titleIsPresent());
		preEmailPage.setEmailAndClickNext(email);
		
		humanPage.titleIsPresent();
		humanPage.clickNextButton();
	}

	@Dado("^que o usuário esta na pagina do Pet$")
	public void que_o_usuário_esta_na_pagina_do_Pet() throws Throwable {

		petPage.titleIsPresent();
	}

	@Dado("^informou sexo como \"([^\"]*)\"$")
	public void informou_sexo_como(String sexo) throws Throwable {
		
		pet = new Pet();
		pet.setGender(sexo);		
		boolean selSexo = petPage.selectGender(sexo);
		Assert.assertTrue(selSexo);
		
	}

	@Dado("^informou quando nasceu \"([^\"]*)\"$")
	public void informou_quando_nasceu(String dataNasc) throws Throwable {
		
		pet.setBirthMonth(dataNasc);
		petPage.setBirthMonth(dataNasc);

	}

	@Dado("^informou raça \"([^\"]*)\"$")
	public void informou_raça(String raca) throws Throwable {

		breed = new Breed();
		breed.setName(raca);
		pet.setBreed(breed);
		petPage.setBreedName(raca);		
		
		String breedName = petPage.getBreedName();
		Assert.assertEquals(raca, breedName);
	}

	@Dado("^informou quilo \"([^\"]*)\"$")
	public void informou_quilo(String quilo) throws Throwable {
		
		valorQuiloEmGramas = Integer.parseInt(quilo);
		petPage.selectWeightKG(quilo);
		
	}

	@Dado("^informou gramas \"([^\"]*)\"$")
	public void informou_gramas(String grama) throws Throwable {

		quiloEmGramas = (valorQuiloEmGramas * 1000) + Integer.parseInt(grama);
		petPage.selectWeightGR(grama);
		pet.setWeightInGrams(Integer.toString(quiloEmGramas));
	}

	@Dado("^informou quantas vezes come por dia \"([^\"]*)\"$")
	public void informou_quantas_vezes_come_por_dia(String comePorDia) throws Throwable {

		pet.setMealsPerDay(comePorDia);
		petPage.clicktMealsPerDay(comePorDia);
		
	}

	@Dado("^informou porte \"([^\"]*)\"$")
	public void informou_porte(String porte) throws Throwable {

		pet.setShape(porte);
		petPage.selectShape(porte);
	}

	@Dado("^informou quilo ideal \"([^\"]*)\"$")
	public void informou_quilo_ideal(String quiloIdeal) throws Throwable {

		valorQuiloIdealEmGramas = Integer.parseInt(quiloIdeal);
		petPage.selectIdealWeightKG(quiloIdeal);
	}

	@Dado("^informou gramas ideal \"([^\"]*)\"$")
	public void informou_gramas_ideal(String gramaIdeal) throws Throwable {
		
		quiloEmGramas = (valorQuiloIdealEmGramas * 1000) + Integer.parseInt(gramaIdeal);
		petPage.selectIdealWeightGR(gramaIdeal);
		pet.setIdealWeightInGrams(Integer.toString(quiloEmGramas));
	}
	
	@Dado("^informou a proteina preferida \"([^\"]*)\"$")
	public void informou_a_proteina_preferida(String proteina) throws Throwable {
		
		pet.setPreferredProtein(proteina);
		boolean protein = petPage.clickPreferedProtein(proteina);
		
		Assert.assertTrue(protein);
	}

	@Dado("^informou que é castrado \"([^\"]*)\"$")
	public void informou_que_é_castrado(String castrado) throws Throwable {

		pet.setCastrated(castrado);
		boolean isCastrated = petPage.isCastrated(castrado);
		
		Assert.assertTrue(isCastrated);
		
	}

	@Dado("^informou que tem problema de saúde \"([^\"]*)\"$")
	public void informou_que_tem_problema_de_saúde(String temProblema) throws Throwable {

		pet.setHealthProblems(temProblema);
		boolean problem = petPage.hasHealthProblems(temProblema);
		
		Assert.assertTrue(problem);
	}

	@Dado("^informou os problemas$")
	public void informou_os_problemas(List<String> listaProblemas) throws Throwable {

		healthProblemsDetails = new HealthProblemsDetails();
		healthProblemsDetails.setProblems(listaProblemas);
				
		boolean problems = petPage.selectProblems(listaProblemas);
		
		Assert.assertTrue(problems);		
	}

	@Dado("^informou a descrição do problema \"([^\"]*)\"$")
	public void informou_a_descrição_do_problema(String descricao) throws Throwable {

		healthProblemsDetails.setDescription(descricao);
		pet.setHealthProblemsDetails(healthProblemsDetails);
		
		petPage.setDescription(descricao);
	}

	@Dado("^anexou um arquivo \"([^\"]*)\"$")
	public void anexou_um_arquivo(String arquivo) throws Throwable {

	}

	@Então("^devo visualizar as frases no gênero masculino$")
	public void devo_visualizar_as_frases_no_gênero_masculino() throws Throwable {

	}

	@Então("^visualizar o resumo dos dados informados$")
	public void visualizar_o_resumo_dos_dados_informados() throws Throwable {

	}

	@Então("^realizar o cadastro com sucesso$")
	public void realizar_o_cadastro_com_sucesso() throws Throwable {

	}

	@Dado("^que o usuário tenha cadastrado cinco pets na pagina de humano$")
	public void que_o_usuário_tenha_cadastrado_cinco_pets_na_pagina_de_humano() throws Throwable {

	}

	@Dado("^informou os sexos$")
	public void informou_os_sexos(DataTable dadosSexo) throws Throwable {

	}

	@Dado("^informou quando nasceram$")
	public void informou_quando_nasceram(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou raças$")
	public void informou_raças(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou quilos$")
	public void informou_quilos(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou gramas$")
	public void informou_gramas(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou quantas vezes come por dia$")
	public void informou_quantas_vezes_come_por_dia(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou portes$")
	public void informou_portes(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou quilos atuais$")
	public void informou_quilos_atuais(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou que é castrado$")
	public void informou_que_é_castrado(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou que tem problema de saúde$")
	public void informou_que_tem_problema_de_saúde(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou que tem os problemas$")
	public void informou_que_tem_os_problemas(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^informou a descrição do problema$")
	public void informou_a_descrição_do_problema(DataTable tabelaDados) throws Throwable {

	}

	@Dado("^anexou arquivos$")
	public void anexou_arquivos(DataTable tabelaDados) throws Throwable {

	}

	@Então("^realizar os  cadastros com sucesso$")
	public void realizar_os_cadastros_com_sucesso() throws Throwable {

	}

	@Dado("^informou com problema de saúde$")
	public void informou_com_problema_de_saúde() throws Throwable {

	}

	@Dado("^não selecionou os problemas listados$")
	public void não_selecionou_os_problemas_listados() throws Throwable {

	}

	@Dado("^informou todos os outros dados obrigatórios$")
	public void informou_todos_os_outros_dados_obrigatórios() throws Throwable {

	}

	@Então("^não deve registrar as informações$")
	public void não_deve_registrar_as_informações() throws Throwable {

	}

	@Então("^deve exibir uma mensagem de campo obrigatório$")
	public void deve_exibir_uma_mensagem_de_campo_obrigatório() throws Throwable {

	}

	@Dado("^informou todos os dados obrigatórios$")
	public void informou_todos_os_dados_obrigatórios() throws Throwable {

	}

	@Dado("^anexou um arquivo$")
	public void anexou_um_arquivo() throws Throwable {

	}

	@Quando("^excluir o arquivo$")
	public void excluir_o_arquivo() throws Throwable {

	}

	@Então("^o arquivo deve ser removido do cadastro$")
	public void o_arquivo_deve_ser_removido_do_cadastro() throws Throwable {

	}

}
