package br.com.jungle.stepdefinitions;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import com.google.gson.Gson;

import br.com.jungle.dao.Attributes;
import br.com.jungle.dao.Ingredients;
import br.com.jungle.dao.LevelsOfGuarantee;
import br.com.jungle.dao.Price;
import br.com.jungle.dao.Products;
import br.com.jungle.report.ExtentCucumberFormatter;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CatalogsAPISteps {
	
	//Variaveis
	private Response response;
	private RequestSpecification request;
	private String URL_BASE = "http://localhost:8083/";
	private String URL;
	private JsonPath jPath;
	
	private Products products;
	private Products getProductFromJson;
	String responseJson;
	
	@Dado("^que informe os dados do produto$")
	public void que_informe_os_dados_do_produto(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);	

		products = new Products();
		products.setCaloriesPerGram(dados.get(0).get("caloriasPorGrama"));
		products.setCategory(dados.get(0).get("categoria"));
		products.setDescription(dados.get(0).get("descricao"));
		products.setId(dados.get(0).get("id"));
		products.setName(dados.get(0).get("nome"));
		products.setPetShape(dados.get(0).get("porte"));
		products.setPrincipalCarbohydrate(dados.get(0).get("carboPrincipal"));
		products.setPrincipalProtein(dados.get(0).get("protPrincipal"));
		products.setSku(dados.get(0).get("sku"));
		products.setTechnicalDescription(dados.get(0).get("descricaoTecnica"));
		products.setTypeProduct(dados.get(0).get("tipoProduto"));
			
		ExtentCucumberFormatter.insertInfoTextInStepReport("Dados do produto: " + dados.get(0).get("caloriasPorGrama")
				+ ", " + dados.get(0).get("categoria")
				+ ", " + dados.get(0).get("descricao")
				+ ", " + dados.get(0).get("id")
				+ ", " + dados.get(0).get("nome")
				+ ", " + dados.get(0).get("porte")
				+ ", " + dados.get(0).get("carboPrincipal")
				+ ", " + dados.get(0).get("protPrincipal")
				+ ", " + dados.get(0).get("sku")
				+ ", " + dados.get(0).get("descricaoTecnica")
				+ ", " + dados.get(0).get("tipoProduto"));
		
	}

	@Dado("^informe os paises disponiveis$")
	public void informe_os_paises_disponiveis(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);	

		List<String> listaPaises = new ArrayList<String>();
		listaPaises.add(dados.get(0).get("paises"));
		products.setCountriesAvailable(listaPaises);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Paises: " + dados.get(0).get("paises"));
	}

	@Dado("^informe os atributos$")
	public void informe_os_atributos(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);
		
		Attributes atributos = new Attributes();
		atributos.setId(dados.get(0).get("id"));
		atributos.setName(dados.get(0).get("nome"));
		atributos.setValue(dados.get(0).get("valor"));
		List<Attributes> listaAtributos = new ArrayList<Attributes>();
		listaAtributos.add(atributos);
		products.setAttributes(listaAtributos);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Atributos: " + dados.get(0).get("id")
				+ ", " + dados.get(0).get("nome")
				+ ", " + dados.get(0).get("valor"));

	}

	@Dado("^informe os enriquecimentos$")
	public void informe_os_enriquecimentos(List<String> listaDados) throws Throwable {
				
		List<String> listaEnriquecimentos = new ArrayList<String>();
		listaEnriquecimentos.add(listaDados.get(0));
		listaEnriquecimentos.add(listaDados.get(1));
		
		products.setEnrichments(listaEnriquecimentos);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Enriquecimentos: " + listaDados.get(0)
				+ ", " + listaDados.get(1));
		
	}

	@Dado("^informe os ingredientes$")
	public void informe_os_ingredientes(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);

		Ingredients ingredientes = new Ingredients();
		ingredientes.setDescription(dados.get(0).get("descricao"));
		ingredientes.setWeightInGrams(dados.get(0).get("pesoEmGramas"));
		List<Ingredients> listaIngredientes = new ArrayList<Ingredients>();
		listaIngredientes.add(ingredientes);
		
		products.setIngredients(listaIngredientes);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Ingredientes: " + dados.get(0).get("descricao")
				+ ", " + dados.get(0).get("pesoEmGramas"));
		
	}

	@Dado("^informe os niveis de garantia$")
	public void informe_os_niveis_de_garantia(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);

		LevelsOfGuarantee nivelDeGarantia = new LevelsOfGuarantee();
		nivelDeGarantia.setDescription(dados.get(0).get("descricao"));
		nivelDeGarantia.setPercentage(dados.get(0).get("porcentagem"));
		List<LevelsOfGuarantee> listNivelGarantia = new ArrayList<LevelsOfGuarantee>();
		listNivelGarantia.add(nivelDeGarantia);
		
		products.setLevelsOfGuarantee(listNivelGarantia);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Niveis de Garantia: " + dados.get(0).get("descricao")
				+ ", " + dados.get(0).get("porcentagem"));
	}

	@Dado("^informe a fase da vida$")
	public void informe_a_fase_da_vida(List<String> listaDados) throws Throwable {
		
		List<String> faseDaVida = new ArrayList<String>();
		faseDaVida.add(listaDados.get(0));

		products.setPhasesOfLife(faseDaVida);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Fases da vida: " + listaDados.get(0));
	}

	@Dado("^informe o preco$")
	public void informe_o_preco(DataTable tabelaDados) throws Throwable {
		
		List<Map<String, String>> dados = tabelaDados.asMaps(String.class, String.class);
		
		Price price = new Price();
		price.setListPriceInCents(dados.get(0).get("listaPrecoEmCentavos"));
		price.setSalePriceInCents(dados.get(0).get("precoVendaEmCentavos"));
		
		products.setPrice(price);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Preco: " + dados.get(0).get("listaPrecoEmCentavos")
				+ ", " + dados.get(0).get("precoVendaEmCentavos"));
	}

	@Dado("^informe as principais caracteristicas$")
	public void informe_as_principais_caracteristicas(List<String> listaDados) throws Throwable {
		
		List<String> listaCaracteristicas = new ArrayList<String>();
		listaCaracteristicas.add(listaDados.get(0));
		
		products.setPrincipalFeatures(listaCaracteristicas);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Principais caracteristicas: " + listaDados.get(0));

	}

	@Dado("^informar a url de produtos \"([^\"]*)\"$")
	public void informar_a_url_de_produtos(String url) throws Throwable {
		
		URL = URL_BASE + url;

	}

	@Quando("^submeter a requisicao POST de produto$")
	public void submeter_a_requisicao_post_de_produto() throws Throwable {
		
		Gson gson = new Gson();
		String requestJson = gson.toJson(products);

		System.out.println("Json criado: " + requestJson);
		
		request = given().contentType(ContentType.JSON).header("country", "BR").body(requestJson);

		response = request.when().post(URL);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + URL);
		
	}
	
	@Quando("^submeter a requisicao GET de produto$")
	public void submeter_a_requisicao_get_de_produto() throws Throwable {
		
		URL = URL + "/" + products.getId();
				
		request = given().header("country", "BR");
		response = request.when().get(URL);

		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + URL);		
		
	}
	
	@Então("^deve receber o codigo da requisicao \"([^\"]*)\"$")
	public void deveReceberOCodigoDaRequisicao(String codigo) throws Throwable {

		response.then().statusCode(Integer.parseInt(codigo));
		String getResponse = response.getBody().asString();

		ExtentCucumberFormatter.insertInfoTextInStepReport("Status Code: " + getResponse);
	}

	@Então("^receber a mensagem de erro \"([^\"]*)\"$")
	public void receberAMensagemDeErro (String mensagem) throws Throwable {

		String message = response.getBody().asString();

		System.out.println("Mensagem de erro completo: " + message);

		jPath = response.jsonPath();

		String defaultMessage = jPath.get("fieldErrors[0].defaultMessage");
		
		System.out.println("Mensagem de erro default: " + defaultMessage);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + defaultMessage);

		assertThat(defaultMessage, equalTo(mensagem));

	}
	
	@Então("^deve receber o json de retorno$")
	public void deve_receber_o_json_de_retorno() throws Throwable {
		
		String responseJson = response.getBody().asString();
		
		Gson gson = new Gson();
		getProductFromJson = new Products();
		getProductFromJson = gson.fromJson(responseJson, Products.class);
	
		System.out.println("Json retornado: " + responseJson);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json retornado pela requisicao GET: " + responseJson);	
				
	}

	@Então("^validar os dados informados do produto$")
	public void validar_os_dados_informados_do_produto() throws Throwable {

		assertThat(Double.parseDouble(products.getCaloriesPerGram()), equalTo(Double.parseDouble(getProductFromJson.getCaloriesPerGram())));
		assertThat(products.getCategory(), equalTo(getProductFromJson.getCategory()));
		assertThat(products.getName(), equalTo(getProductFromJson.getName()));
		assertThat(products.getPetShape(), equalTo(getProductFromJson.getPetShape()));
		assertThat(products.getId(), equalTo(getProductFromJson.getId()));
		assertThat(products.getPrincipalCarbohydrate(), equalTo(getProductFromJson.getPrincipalCarbohydrate()));
		assertThat(products.getPrincipalFeatures().get(0), equalTo(getProductFromJson.getPrincipalFeatures().get(0)));		
		assertThat(products.getPrincipalProtein(), equalTo(getProductFromJson.getPrincipalProtein()));		
		assertThat(products.getSku(), equalTo(getProductFromJson.getSku()));		
		assertThat(products.getTechnicalDescription(), equalTo(getProductFromJson.getTechnicalDescription()));		
		assertThat(products.getTypeProduct(), equalTo(getProductFromJson.getTypeProduct()));
		assertThat(products.getDescription(), equalTo(getProductFromJson.getDescription()));
		assertThat(products.getPrice().getListPriceInCents(), equalTo(getProductFromJson.getPrice().getListPriceInCents()));
		assertThat(products.getPrice().getSalePriceInCents(), equalTo(getProductFromJson.getPrice().getSalePriceInCents()));
		
		/**
		 * Apos correcao do pais como nulo descomentar esse assert
		 */
		//assertThat(products.getCountriesAvailable().get(0), equalTo(getProductFromJson.getCountriesAvailable().get(0)));
		
		assertThat(products.getEnrichments(), contains(getProductFromJson.getEnrichments().get(1), getProductFromJson.getEnrichments().get(0)));
				
		assertThat(products.getIngredients().get(0).getDescription(), equalTo(getProductFromJson.getIngredients().get(0).getDescription()));
		assertThat(products.getIngredients().get(0).getWeightInGrams(), equalTo(getProductFromJson.getIngredients().get(0).getWeightInGrams()));
		
		assertThat(products.getLevelsOfGuarantee().get(0).getDescription(), equalTo(getProductFromJson.getLevelsOfGuarantee().get(0).getDescription()));
		assertThat(Double.parseDouble(products.getLevelsOfGuarantee().get(0).getPercentage()), equalTo(Double.parseDouble(getProductFromJson.getLevelsOfGuarantee().get(0).getPercentage())));
		assertThat(products.getPhasesOfLife().get(0), equalTo(getProductFromJson.getPhasesOfLife().get(0)));
		
		assertThat(products.getAttributes().get(0).getId(), equalTo(getProductFromJson.getAttributes().get(0).getId()));
		assertThat(products.getAttributes().get(0).getName(), equalTo(getProductFromJson.getAttributes().get(0).getName()));
		assertThat(products.getAttributes().get(0).getValue(), equalTo(getProductFromJson.getAttributes().get(0).getValue()));
		
	}

}
