package br.com.jungle.stepdefinitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.github.javafaker.Faker;
import com.google.gson.Gson;

import br.com.jungle.dao.Breed;
import br.com.jungle.dao.Cellphone;
import br.com.jungle.dao.HealthProblemsDetails;
import br.com.jungle.dao.Human;
import br.com.jungle.dao.Pet;
import br.com.jungle.dao.UploadFile;
import br.com.jungle.report.ExtentCucumberFormatter;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TutorsAPISteps {

	//Variaveis
	private Response response;
	private RequestSpecification request;
	private String URL_BASE = "http://localhost:8081/";
	
	JsonPath jPath;
	Gson gson = new Gson();
	Faker faker = new Faker();

	ArrayList<String> listRegisterHuman = new ArrayList<String>();
	List<String> problems;
	ArrayList<Pet> listPets;
	List<Map<String, String>> tableHeathProblems;

	Cellphone cellphone = new Cellphone();
	Human human = new Human();
	Breed breed;
	HealthProblemsDetails healthProblemsDetails;
	Pet pet;
	UploadFile uploadFile;
	File file;

	private String getResponse;
	private String emailFaker;
	private String uuid;
	private String postUrl;
	private String putUrl;
	private String getUrl;
	private String getRegisterJson;
	
	// Problemas
	private String healthProblems;
	private String allergy;
	private String foodallergy;
	private String cardiology;
	private String diabetes;
	private String gastrointestinal;
	private String renal;
	private String other;
	private String description;
	
	// Dados da raca
	private String initialMonth;
	private String finalMonth;
	private String phaseOfLife;
	
	// Dados do humano
	private String email;
	private String name;
	private String lastName;
	private String zipCode;
	private String area;
	private String number;
	private String type;
	private String howToKnowEC;
	private String petStatus;
	private String status;
	
	//Dados do Pet
	private String idPet;
	private String gender;
	private String birthMonth;
	private String ageInMonths;
	private String idBreed;
	private String breedName;
	private String weightInGrams;
	private String idealWeightInGrams;
	private String mealsPerDay;
	private String preferredProtein;
	private String shape;
	private String castrated;
	private String sizeOfBreed;
	private String adultIdealWeightInGrams;
	
	// ******************************************************************Check E-mail************************************************************************************//
	@Dado("^que o e-mail nao seja cadastrado$")
	public void queOEmailNaoSejaCadastrado() throws Throwable {

		emailFaker = faker.internet().emailAddress();
		ExtentCucumberFormatter.insertInfoTextInStepReport("Email: " + emailFaker);
	}

	@Dado("^informar o e-mail na api$")
	public void informarOEmailNaApi() throws Throwable {

		String emailJson = "{\"email\":\"" + emailFaker + "\"}";

		request = given().contentType(ContentType.JSON).header("country", "BR").body(emailJson);

		request.log().body();
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json: " + emailJson);
	}

	@Dado("^informar a url check-email$")
	public void informarAUrlCheckEmail() throws Throwable {

		postUrl = URL_BASE + "check-email";

		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Check E-mail: " + postUrl);
	}

	@Quando("^submeter a requisicao POST$")
	public void submeterARequisicaoPost() throws Throwable {
		response = request.when().post(postUrl);
	}

	@Então("^deve receber o codigo \"([^\"]*)\"$")
	public void deveReceberOCodigo(int codigo) throws Throwable {

		response.then().statusCode(codigo);
		getResponse = response.getBody().asString();

		ExtentCucumberFormatter.insertInfoTextInStepReport("Status Code: " + getResponse);
	}

	@Então("^retornar o codigo uuid$")
	public void retornarOCodigoUuid() throws Throwable {

		jPath = response.jsonPath();
		uuid = jPath.get("uuid");

		System.out.println("UUID: " + uuid);

		ExtentCucumberFormatter.insertInfoTextInStepReport("UUID: " + uuid);

		Assert.assertNotNull("O e-mail cadastrado nao retornou o uuid: " + uuid, uuid);

	}

	@Dado("^informar o e-mail invalido na api \"([^\"]*)\"$")
	public void informarOEmailInvalidoNaApi(String email) throws Throwable {

		emailFaker = email;

		String emailJson = "{\"email\":\"" + emailFaker + "\"}";

		request = given().contentType(ContentType.JSON).header("country", "BR").body(emailJson);

		request.log().body();
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json: " + emailJson);
	}

	@Então("^retornar a mensagem de erro \"([^\"]*)\"$")
	public void retornarAMensagemDeErro(String mensagem) throws Throwable {

		String message = response.getBody().asString();

		System.out.println("Mensagem de erro completo: " + message);

		jPath = response.jsonPath();
		String defaultMessage = jPath.get("fieldErrors[0].defaultMessage");

		System.out.println("Mensagem de erro default: " + defaultMessage);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + defaultMessage);

		Assert.assertEquals(mensagem, defaultMessage.contains(mensagem), true);

	}

	// ******************************************************************Exists************************************************************************************//
	@Dado("^que o e-mail seja cadastrado \"([^\"]*)\"$")
	public void queOEmailSejaCadastrado(String email) throws Throwable {

		emailFaker = email;

		human.setEmail(emailFaker);

		System.out.println("E-mail: " + human.getEmail());

		request = given().contentType(ContentType.JSON).header("country", "BR");

		ExtentCucumberFormatter.insertInfoTextInStepReport("E-mail: " + human.getEmail());
	}

	@Dado("^informar a url exists$")
	public void informarAUrlExists() throws Throwable {

		getUrl = URL_BASE + "exists" + "/" + emailFaker;
		System.out.println(getUrl);

		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Exists: " + getUrl);
	}

	@Quando("^submeter a requisicao GET$")
	public void submeterARequisicaoGet() throws Throwable {

		response = request.when().get(getUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + getUrl);

	}

	@Então("^retornar o codigo uuid gerado no cadastro \"([^\"]*)\"$")
	public void retornarOCodigoUuidGeradoNoCadastro(String paramUuid) throws Throwable {

		jPath = response.jsonPath();
		uuid = jPath.get("uuid");

		System.out.println("UUID: " + uuid);

		ExtentCucumberFormatter.insertInfoTextInStepReport("UUID: " + uuid);

		Assert.assertEquals("Os UUIDs: " + paramUuid + "e " + uuid + "nao sao inguais!", paramUuid, uuid);

	}

	@Dado("^informar o e-mail inexistente na api \"([^\"]*)\"$")
	public void informarOEmailInexistenteNaApi(String email) throws Throwable {

		emailFaker = email;

		request = given().contentType(ContentType.JSON).header("country", "BR");

		ExtentCucumberFormatter.insertInfoTextInStepReport("E-mail inexistente: " + emailFaker);

	}

	@Então("^retornar a mensagem de erro Email Nao Encontrado$")
	public void retornarAMensagemDeErroEmailNaoEncontrado() throws Throwable {

		jPath = response.jsonPath();
		String defaultMessage = jPath.get("message");

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + defaultMessage);

		String message = "Tutor " + emailFaker + " " + "not found";

		System.out.println("Mensagem de erro do Json: " + defaultMessage + " ,Mensagem de erro da feature: " + message);

		Assert.assertEquals(message, defaultMessage.contains(message), true);

	}

	// ******************************************************************Complete************************************************************************************//

	@Dado("^informar a url register complete$")
	public void informarAUrlRegisterComplete() throws Throwable {

		getUrl = URL_BASE + uuid + "/complete";
		System.out.println(getUrl);

		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Register: " + getUrl);

		request = given().header("country", "BR");
	}

	@Então("^retornar as fases da vida$")
	public void retornarAsFasesDaVida(DataTable dadosCompleto) throws Throwable {
		
		Human human = gson.fromJson(response.getBody().asString(), Human.class);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json de retorno: " + getRegisterJson);

		List<Map<String, String>> dados = dadosCompleto.asMaps(String.class, String.class);		
		
		for(int i = 0; dados.size() > i; i++) {

			if (dados.get(i).get("mesInicial") != null) {
				initialMonth = human.getPets().get(0).getBreed().getRangesPhaseOfLive().get(i).getInitialMonth();
				Assert.assertEquals("Mes inicial invalido!", dados.get(i).get("mesInicial"), initialMonth);	
			} else {	
				initialMonth = "-";
			}
	
			if (dados.get(i).get("mesFinal") != null) {
				finalMonth = human.getPets().get(0).getBreed().getRangesPhaseOfLive().get(i).getFinalMonth();
				Assert.assertEquals("Mes final invalido!", dados.get(i).get("mesFinal"), human.getPets().get(0).getBreed().getRangesPhaseOfLive().get(i).getFinalMonth());
			} else {
				finalMonth = "-";
			}
	
			if (dados.get(i).get("faseDaVida") != null) {
				phaseOfLife = human.getPets().get(0).getBreed().getRangesPhaseOfLive().get(i).getPhaseOfLife();
				Assert.assertEquals("Fase da vida invalido!", dados.get(i).get("faseDaVida"), phaseOfLife);
			} else {
				phaseOfLife = "-";
			}

			System.out.println("Dados de retorno da fase da vida: /TMes inicial: " + initialMonth + " , Mes final: " + finalMonth + " , Fase da vida: " + phaseOfLife);
			
			ExtentCucumberFormatter.insertInfoTextInStepReport("Dados de retorno da fase da vida: /TMes inicial: " + initialMonth + " , Mes final: " + finalMonth + " , Fase da vida: " + phaseOfLife);
		}
		
	}

	// ******************************************************************Register************************************************************************************//
	@Dado("^informar o codigo UUID gerado \"([^\"]*)\"$")
	public void informarOCodigoUUIDGerado(String paramUuid) throws Throwable {

		uuid = paramUuid;

		ExtentCucumberFormatter.insertInfoTextInStepReport("Codigo UUID: " + uuid);

	}

	@Dado("^informar a url register$")
	public void informarAUrlRegister() throws Throwable {

		getUrl = URL_BASE + uuid + "/register";
		System.out.println(getUrl);

		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Register: " + getUrl);
	}

	@Então("^retornar os registros cadastrados$")
	public void retornarOsRegistrosCadastrados(List<String> listaDados) throws Throwable {
		
		Human human = gson.fromJson(response.getBody().asString(), Human.class);

		System.out.println("Dados de retorno: " + human.getEmail() + " , " + human.getName() + " , " + human.getLastName() + " , " + human.getZipCode() + " , "
				+ human.getCellphone().getArea() + " , " + human.getCellphone().getNumber() + " , " + human.getHowToKnowEC() + " , " + human.getPets().get(0).getName() + " , "
				+ human.getPets().get(0).getStatus() + " , " + human.getStatus());

		getRegisterJson = response.getBody().asString();
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json de retorno: " + getRegisterJson);

		listRegisterHuman.addAll(listaDados);

		Assert.assertEquals("E-mail invalido!", emailFaker, human.getEmail());
		Assert.assertEquals("Nome invalido!", listRegisterHuman.get(0), human.getName());
		Assert.assertEquals("Sobrenome invalido!", listRegisterHuman.get(1), human.getLastName());
		Assert.assertEquals("CEP invalido!", listRegisterHuman.get(2), human.getZipCode());
		Assert.assertEquals("Celular invalido!", listRegisterHuman.get(3) + listRegisterHuman.get(4), human.getCellphone().getArea() + human.getCellphone().getNumber());
		Assert.assertEquals("Como conheceu invalido!", listRegisterHuman.get(5), human.getHowToKnowEC());
		Assert.assertEquals("Nome pet invalido!", listRegisterHuman.get(6), human.getPets().get(0).getName());
		Assert.assertEquals("Status pet invalido!", listRegisterHuman.get(7), human.getPets().get(0).getStatus());
		Assert.assertEquals("Status invalido!", listRegisterHuman.get(8), human.getStatus());

	}

	@Dado("^informar o codigo UUID invalido \"([^\"]*)\"$")
	public void informarOCodigoUUIDInvalido(String paramUuid) throws Throwable {

		uuid = paramUuid;

		ExtentCucumberFormatter.insertInfoTextInStepReport("Codigo UUID: " + uuid);

	}

	@Então("^retornar a mensagem de erro UUID Nao Encontrado$")
	public void retornarAMensagemDeErroUuidNaoEncontrado() throws Throwable {

		jPath = response.jsonPath();
		String defaultMessage = jPath.get("message");

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + defaultMessage);

		String message = "Tutor " + uuid + " (uuid) not found";

		System.out.println("Mensagem de erro do Json: " + defaultMessage + " ,Mensagem de erro da feature: " + message);

		Assert.assertEquals("Mensagem invalida! " + defaultMessage, message, defaultMessage);

	}

	// ******************************************************************RegisterDiet************************************************************************************//

	@Dado("^que informei o codigo UUID \"([^\"]*)\"$")
	public void queInformeiOCodigoUUID(String paramUuid) throws Throwable {

		uuid = paramUuid;

		ExtentCucumberFormatter.insertInfoTextInStepReport("Codigo UUID: " + uuid);
	}

	@Dado("^informar a url register diet$")
	public void informarAUrlRegisterDiet() throws Throwable {

		putUrl = URL_BASE + uuid + "/register/diet";
		System.out.println(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL put do registro da dieta: " + putUrl);
	}

	@Quando("^submeter a requisicao PUT de dieta$")
	public void submeterARequisicaoPutDeDieta() throws Throwable {

		request = given().header("country", "BR");
		response = request.when().put(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + putUrl);

	}

	@Então("^retornar os registros de humano cadastrados$")
	public void retornarOsRegistrosDeHumanoCadastrados(DataTable dadosHumano) throws Throwable {
		
		System.out.println(response.getBody().asString());
		
		Human human = gson.fromJson(response.getBody().asString(), Human.class);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json de retorno: " + getRegisterJson);

		List<Map<String, String>> dados = dadosHumano.asMaps(String.class, String.class);

		// Validacao do retorno dos dados do Humano
		if (dados.get(0).get("email") != null) {
			email = human.getEmail();
			Assert.assertEquals("E-mail invalido!", dados.get(0).get("email"), email);
		}else {
			email = "-";
		}

		if (dados.get(0).get("nome") != null) {
			name = human.getName();
			Assert.assertEquals("Nome invalido!", dados.get(0).get("nome"), name);
		}else {
			name = "-";
		}

		if (dados.get(0).get("sobrenome") != null) {
			lastName = human.getLastName();
			Assert.assertEquals("Sobrenome invalido!", dados.get(0).get("sobrenome"), lastName);
		}else {
			lastName = "-";
		}

		if (dados.get(0).get("cep") != null) {
			zipCode = human.getZipCode();
			Assert.assertEquals("CEP invalido!", dados.get(0).get("cep"), zipCode);
		}else {
			zipCode = "-";
		}

		if (dados.get(0).get("ddd") != null || dados.get(0).get("numero") != null) {
			area = human.getCellphone().getArea();
			number = human.getCellphone().getNumber();
			Assert.assertEquals("Celular invalido!", dados.get(0).get("ddd") + dados.get(0).get("numero"), area + number);
		}else {
			area = "-";
			number = "-";
		}

		if (dados.get(0).get("tipo") != null) {
			type = human.getCellphone().getType();
			Assert.assertEquals("Tipo telefone invalido!", dados.get(0).get("tipo"), type);
		}else {
			type = "-";
		}

		if (dados.get(0).get("comoConheceu") != null) {
			howToKnowEC = human.getHowToKnowEC();
			Assert.assertEquals("Como conheceu invalido!", dados.get(0).get("comoConheceu"), howToKnowEC);
		}else {
			howToKnowEC = "-";
		}
		
		if (dados.get(0).get("status") != null) {
			petStatus = human.getPets().get(0).getStatus();
			Assert.assertEquals("Status registro invalido!", dados.get(0).get("status"), petStatus);
		}else {
			petStatus = "-";
		}

		if (dados.get(0).get("statusRegistro") != null) {
			status = human.getStatus();
			Assert.assertEquals("Status invalido!", dados.get(0).get("statusRegistro"), status);
		}else {
			status = "-";
		}

		System.out.println("Dados de retorno do humano: /E-mail: " + email + " , Nome: " + name + " , Sobrenome: " + lastName + " , CEP: " + zipCode
				+ " , DDD: " + area + " , Numero: " + number + " , Tipo: " + type + " , Como conheceu: " + howToKnowEC + " , Status: " + petStatus + " , Status Registro: " + status);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Dados de retorno do humano: /E-mail: " + email + " , Nome: " + name + " , Sobrenome: " + lastName + " , CEP: " + zipCode
				+ " , DDD: " + area + " , Numero: " + number + " , Tipo: " + type + " , Como conheceu: " + howToKnowEC + " , Status: " + petStatus + " , Status Registro: " + status);
	}
	
	@Então("^retornar os registros do pet cadastrado$")
	public void retornarOsRegistrosDoPetCadastrado(DataTable dadosPet) throws Throwable {
		
		Human hPet = gson.fromJson(response.getBody().asString(), Human.class);
		
		System.out.println("json: " + response.getBody().asString());

		List<Map<String,String>> dados = dadosPet.asMaps(String.class,String.class);
		
		//Validacao do retorno dos dados do Pet
		if (dados.get(0).get("idPet") != null) {
			idPet = hPet.getPets().get(0).getId();
			Assert.assertEquals("PetId invalido!", dados.get(0).get("idPet"), idPet);
		}else {
			idPet = "-";
		}
		
		if (dados.get(0).get("nomePet") != null) {
			name = hPet.getPets().get(0).getName();
			Assert.assertEquals("Nome do pet invalido!", dados.get(0).get("nomePet"), name);
		}else {
			name = "-";
		}
		
		if (dados.get(0).get("sobrenomePet") != null) {
			lastName = hPet.getPets().get(0).getLastName();
			Assert.assertEquals("Sobrenome do pet invalido!", dados.get(0).get("sobrenomePet"), lastName);
		}else {
			lastName = "-";
		}
		
		if (dados.get(0).get("sexo") != null) {
			gender = hPet.getPets().get(0).getGender();
			Assert.assertEquals("Sexo invalido!", dados.get(0).get("sexo"), gender);
		}
		else {
			gender = "-";
		}
		
		if (dados.get(0).get("problemaDeSaude") != null) {
			healthProblems = hPet.getPets().get(0).getHealthProblems();
			Assert.assertEquals("Tem problema de saude invalido!", dados.get(0).get("problemaDeSaude"), healthProblems);
		}else {
			healthProblems = "-";
		}
		
		if (dados.get(0).get("dataNasc") != null) {
			birthMonth = hPet.getPets().get(0).getBirthMonth();
			Assert.assertEquals("Data Nascimento invalido!", dados.get(0).get("dataNasc"), birthMonth);
		}else {
			birthMonth = "-";
		}
		
		if (dados.get(0).get("idadeEmMeses") != null) {
			ageInMonths = hPet.getPets().get(0).getAgeInMonths();
			Assert.assertEquals("Idade em Meses invalido!", dados.get(0).get("idadeEmMeses"), ageInMonths);
		}else {
			ageInMonths = "-";
		}
		
		if (dados.get(0).get("idRaca") != null) {
			idBreed = hPet.getPets().get(0).getBreed().getId();
			Assert.assertEquals("ID da Raca invalida!", dados.get(0).get("idRaca"), idBreed);
		}else {
			idBreed = "-";
		}
		
		if (dados.get(0).get("raca") != null) {
			breedName = hPet.getPets().get(0).getBreed().getName();
			Assert.assertEquals("Raca invalida!", dados.get(0).get("raca"), breedName);
		}else {
			breedName = "-";
		}
		
		if (dados.get(0).get("pesoEmGramas") != null) {
			weightInGrams = hPet.getPets().get(0).getWeightInGrams();
			Assert.assertEquals("Peso atual invalido!", dados.get(0).get("pesoEmGramas"), weightInGrams);
		}else {
			weightInGrams = "-";
		}
		
		if (dados.get(0).get("pesoIdealEmGramas") != null) {
			idealWeightInGrams = hPet.getPets().get(0).getIdealWeightInGrams();
			Assert.assertEquals("Peso Ideal invalido!", dados.get(0).get("pesoIdealEmGramas"), idealWeightInGrams);
		}else {
			idealWeightInGrams = "-";
		}
		
		if (dados.get(0).get("comePorDia") != null) {
			mealsPerDay = hPet.getPets().get(0).getMealsPerDay();
			Assert.assertEquals("Come quantas vezes por dia invalido!", dados.get(0).get("comePorDia"), mealsPerDay);
		}else {
			mealsPerDay = "-";
		}
		
		if (dados.get(0).get("proteina") != null) {
			preferredProtein = hPet.getPets().get(0).getPreferredProtein();
			Assert.assertEquals("Proteina invalido!", dados.get(0).get("proteina"), preferredProtein);
		}else {
			preferredProtein = "-";
		}
		
		if (dados.get(0).get("porte") != null) {
			shape = hPet.getPets().get(0).getShape();
			Assert.assertEquals("Porte invalido!", dados.get(0).get("porte"), shape);
		}else {
			shape = "-";
		}
		
		if (dados.get(0).get("castrado") != null) {
			castrated = hPet.getPets().get(0).getCastrated();
			Assert.assertEquals("É castrado invalido!", dados.get(0).get("castrado"), castrated);	
		}else {
			castrated = "-";
		}
		
		if (dados.get(0).get("tamanhoDaRaca") != null) {
			sizeOfBreed = hPet.getPets().get(0).getBreed().getSizeOfBreed();
			Assert.assertEquals("Tamanho da raca invalido!", dados.get(0).get("tamanhoDaRaca"), sizeOfBreed);	
		}else {
			sizeOfBreed = "-";
		}
		
		if (dados.get(0).get("pesoIdealAdulto") != null) {
			adultIdealWeightInGrams = hPet.getPets().get(0).getBreed().getAdultIdealWeightInGrams();
			Assert.assertEquals("Peso ideal adulto invalido!", dados.get(0).get("pesoIdealAdulto"), adultIdealWeightInGrams);	
		}else {
			adultIdealWeightInGrams = "-";
		}	
				
		System.out.println("Dados de retorno do pet: /Id Pet: " + idPet + " , Nome Pet: " + name + " , Sobrenome Pet: " + lastName + " , Sexo: " + gender + " , Tem problema de saude?: " + healthProblems
				+ " , Data Nasc: " + birthMonth + " , Idade em meses: " + ageInMonths + " , Id Raca: " + idBreed + " , Nome da raca: " + breedName + " , Peso em gramas: " + weightInGrams
				+ " , Peso ideal em gramas: " + idealWeightInGrams + " , Come por dia: " + mealsPerDay + " , Proteina preferida: " + preferredProtein + " , Porte: " + shape + " , Castrado: " + castrated 
				+ " , Tamanho da raca: " + sizeOfBreed + " , Peso ideal em gramas: " + adultIdealWeightInGrams);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Dados de retorno do pet: /Id Pet: " + idPet + " , Nome Pet: " + name + " , Sobrenome Pet: " + lastName + " , Sexo: " + gender + " , Tem problema de saude?: " + healthProblems
				+ " , Data Nasc: " + birthMonth + " , Idade em meses: " + ageInMonths + " , Id Raca: " + idBreed + " , Nome da raca: " + breedName + " , Peso em gramas: " + weightInGrams
				+ " , Peso ideal em gramas: " + idealWeightInGrams + " , Come por dia: " + mealsPerDay + " , Proteina preferida: " + preferredProtein + " , Porte: " + shape + " , Castrado: " + castrated 
				+ " , Tamanho da raca: " + sizeOfBreed + " , Peso ideal em gramas: " + adultIdealWeightInGrams);
	}

	@Então("^se tem problema de saude retornar os problemas cadastrados$")
	public void seTemProblemaDeSaudeRetornarOsProblemasCadastrados(DataTable dadosProblema) throws Throwable {
		
		Human hProblems = gson.fromJson(response.getBody().asString(), Human.class);

		List<Map<String, String>> dados = dadosProblema.asMaps(String.class, String.class);

		// Validacao do retorno dos problemas de saude
		if (dados.get(0).get("alergia") != null) {
			allergy = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(0);
			Assert.assertEquals("Alergia invalido!", dados.get(0).get("alergia"), allergy);
		}else {			
			allergy = "-";
		}
		
		if (dados.get(0).get("alergiaAlimentar") != null) {
			foodallergy = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(1);
			Assert.assertEquals("Alergia Alimentar invalido!", dados.get(0).get("alergiaAlimentar"), foodallergy);
		}else {			
			foodallergy = "-";
		}
		
		if (dados.get(0).get("cardiologica") != null) {
			cardiology = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(2);
			Assert.assertEquals("Cardiologico invalido!", dados.get(0).get("cardiologica"), cardiology);
		}else {			
			cardiology = "-";
		}
		
		if (dados.get(0).get("diabetes") != null) {
			diabetes = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(3);
			Assert.assertEquals("Diabetes invalido!", dados.get(0).get("diabetes"), diabetes);
		}else {			
			diabetes = "-";
		}
		
		if (dados.get(0).get("gastrointestinal") != null) {
			gastrointestinal = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(4);
			Assert.assertEquals("Gastrointestinal invalido!", dados.get(0).get("gastrointestinal"), gastrointestinal);
		}else {			
			gastrointestinal = "-";
		}
		
		if (dados.get(0).get("renal") != null) {
			renal = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(5);
			Assert.assertEquals("Renal invalido!", dados.get(0).get("renal"), renal);
		}else {			
			renal = "-";
		}
		
		if (dados.get(0).get("outros") != null) {
			other = hProblems.getPets().get(0).getHealthProblemsDetails().getProblems().get(6);
			Assert.assertEquals("Outros invalido!", dados.get(0).get("outros"), other);
		}else {			
			other = "-";
		}	
		
		if (dados.get(0).get("descricao") != null) {
			description = hProblems.getPets().get(0).getHealthProblemsDetails().getDescription();
			Assert.assertEquals("Descricao do problema invalido!", dados.get(0).get("descricao"), description);
		}else {			
			description = "-";
		}
				
		System.out.println("Dados de retorno de problemas: /Descricao do Problema: " + description 
				+ " Problemas: " + allergy + " , " + foodallergy + " , " + cardiology + " , " + diabetes + " , " + gastrointestinal
				+ " , " + renal + " , " + other);
		
		ExtentCucumberFormatter.insertInfoTextInStepReport("Dados de retorno de problemas: /Descricao do Problema: " + description 
				+ " Problemas: " + allergy + " , " + foodallergy + " , " + cardiology + " , " + diabetes + " , " + gastrointestinal
				+ " , " + renal + " , " + other);
	}

	// ******************************************************************RegisterHuman************************************************************************************//
	@Dado("^informar o Nome \"([^\"]*)\"$")
	public void informarONome(String nome) throws Throwable {

		human.setName(nome);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Nome: " + human.getName());
	}

	@Dado("^informar o Sobrenome \"([^\"]*)\"$")
	public void informarOSobrenome(String sobrenome) throws Throwable {

		human.setLastName(sobrenome);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Sobrenome: " + human.getLastName());
	}

	@Dado("^informar o CEP \"([^\"]*)\"$")
	public void informarOCEP(String cep) throws Throwable {

		human.setZipCode(cep);
		ExtentCucumberFormatter.insertInfoTextInStepReport("CEP: " + human.getZipCode());
	}

	@Dado("^informar se o CEP faz parte da regiao de entrega \"([^\"]*)\"$")
	public void informarSeOCEPFazParteDaRegiaoDeEntrega(String entrega) throws Throwable {

		human.setZipCodeCovered(entrega);
		ExtentCucumberFormatter
				.insertInfoTextInStepReport("CEP faz parte da regiao de entrega?: " + human.getZipCodeCovered());
	}

	@Dado("^informar o Numero do Celular \"([^\"]*)\", \"([^\"]*)\" e \"([^\"]*)\"$")
	public void informarONumeroDoCelular(String ddd, String numero, String tipo) throws Throwable {

		cellphone.setArea(ddd);
		cellphone.setNumber(numero);
		cellphone.setType(tipo);
		human.setCellphone(cellphone);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Telefone: " + human.getCellphone().getArea()
				+ human.getCellphone().getNumber() + human.getCellphone().getType());
	}

	@Dado("^informar como Conheceu a eleven chimps \"([^\"]*)\"$")
	public void informarComoConheceuAElevenChimps(String comoConheceu) throws Throwable {

		human.setHowToKnowEC(comoConheceu);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Como conheceu a ElevenChimps?: " + human.getHowToKnowEC());
	}

	@Dado("^informar os nomes dos pets \"([^\"]*)\"$")
	public void informarOsDadosDoPet(String nomePet) throws Throwable {

		String petId = nomePet + human.getLastName();

		pet = new Pet(petId.toLowerCase(), nomePet);

		System.out.println("Nome do Pet: " + pet.getName());

		listPets = new ArrayList<Pet>();
		listPets.add(pet);

		human.setPets(listPets);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Nome do pet: " + human.getPets().get(0).getName());
	}

	@Dado("^informar a url human register$")
	public void informarAUrlHumanRegister() throws Throwable {

		putUrl = URL_BASE + uuid + "/register/human";
		System.out.println(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Human register: " + putUrl);
	}

	@Quando("^submeter a requisicao PUT de humano$")
	public void submeterARequisicaoPutDeHumano() throws Throwable {

		String requestJson = gson.toJson(human);
		System.out.println("Json criado: " + requestJson);

		response = request.body(requestJson).when().put(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + putUrl);

	}

	@Então("^retornar a mensagem de erro no json \"([^\"]*)\"$")
	public void retornarAMensagemDeErroNoJson(String mensagem) throws Throwable {

		String message = response.getBody().asString();

		System.out.println("Mensagem de erro completo: " + message);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + message);

		Assert.assertEquals("A mensagem: " + mensagem + " nao foi encontrada!", message.contains(mensagem), true);

	}

	// ******************************************************************RegisterPet************************************************************************************//
	@Dado("^informar data nascimento pet \"([^\"]*)\"$")
	public void informarQuandoEleNasceu(String dataNasc) throws Throwable {

		int mesAtual = LocalDate.now().getMonthValue();
		int anoAtual = LocalDate.now().getYear();

		pet = new Pet();

		if (dataNasc.equals("dataAnterior")) {
			String dataAnterior = Integer.toString(mesAtual - 1) + Integer.toString(anoAtual);
			System.out.println(dataAnterior);
			pet.setBirthMonth(dataAnterior);
			ExtentCucumberFormatter.insertInfoTextInStepReport("Data de nascimento Pet: " + pet.getBirthMonth());
		}

		if (dataNasc.equals("dataPosterior")) {
			String dataPosterior = Integer.toString(mesAtual + 1) + Integer.toString(anoAtual);
			System.out.println(dataPosterior);
			pet.setBirthMonth(dataPosterior);
			ExtentCucumberFormatter.insertInfoTextInStepReport("Data de nascimento Pet: " + pet.getBirthMonth());
		}

		pet.setBirthMonth(dataNasc);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Data de nascimento Pet: " + pet.getBirthMonth());
	}

	@Dado("^informar a raca \"([^\"]*)\"$")
	public void informarARaca(String raca) throws Throwable {

		if (raca.equals("idRacaVazio")) {
			breed = new Breed("", "Akita");
		} else {
			breed = new Breed("6", raca);
		}

		pet.setBreed(breed);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Raca: " + pet.getBreed().getName());
	}

	@Dado("^informar se é castrado \"([^\"]*)\"$")
	public void informarSeÉCastrado(String castrado) throws Throwable {

		pet.setCastrated(castrado);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Castrado?: " + pet.getCastrated());
	}

	@Dado("^informar o sexo do pet \"([^\"]*)\"$")
	public void informarOSexoDoPet(String sexo) throws Throwable {

		pet.setGender(sexo);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Sexo: " + pet.getGender());
	}

	@Dado("^informar se tem problema de saude \"([^\"]*)\"$")
	public void informarSeTemProblemaDeSaude(String temProblema) throws Throwable {

		pet.setHealthProblems(temProblema);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Tem problema de saude?: " + pet.getHealthProblems());
	}

	@Dado("^informar a descricao do problema \"([^\"]*)\"$")
	public void informarADescricaoDoProblema(String descricao) throws Throwable {

		healthProblemsDetails = new HealthProblemsDetails();
		healthProblemsDetails.setDescription(descricao);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Detalhes do problema de saude: " + healthProblemsDetails.getDescription());
	}

	@Dado("^informar os problemas$")
	public void informarOsProblemas(List<String> problemas) throws Throwable {

		problems = new ArrayList<String>();

		for (String itemProblem : problemas) {
			problems.add(itemProblem);
			ExtentCucumberFormatter.insertInfoTextInStepReport("Problemas de saude: " + itemProblem);
		}
		healthProblemsDetails.setProblems(problems);
		pet.setHealthProblemsDetails(healthProblemsDetails);

	}

	@Dado("^informar o Nome do Pet \"([^\"]*)\"$")
	public void informarONomeDoPet(String nome) throws Throwable {

		pet.setName(nome);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Nome do Pet: " + pet.getName());
	}

	@Dado("^informar o ID do pet \"([^\"]*)\"$")
	public void informarOIDDoPet(String petId) throws Throwable {

		pet.setId(petId);
		ExtentCucumberFormatter.insertInfoTextInStepReport("ID do Pet: " + pet.getId());
	}

	@Dado("^informar o peso ideal em gramas \"([^\"]*)\"$")
	public void informarOPesoIdealEmGramas(String pesoIdeal) throws Throwable {

		pet.setIdealWeightInGrams(pesoIdeal);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Peso Ideal do Pet: " + pet.getIdealWeightInGrams());
	}

	@Dado("^informar o Sobrenome do pet \"([^\"]*)\"$")
	public void informarOSobrenomeDoPet(String sobrenome) throws Throwable {

		pet.setLastName(sobrenome);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Sobrenome do Pet: " + pet.getLastName());
	}

	@Dado("^informar quantas vezes come por dia \"([^\"]*)\"$")
	public void informarQuantasVezesComePorDia(String qtsVezesComePorDia) throws Throwable {

		pet.setMealsPerDay(qtsVezesComePorDia);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Come quantas vezes por dia: " + pet.getMealsPerDay());
	}

	@Dado("^informar a proteina preferida \"([^\"]*)\"$")
	public void informarAProteinaPreferida(String proteina) throws Throwable {

		pet.setPreferredProtein(proteina);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Proteina preferiada: " + pet.getPreferredProtein());
	}

	@Dado("^informar o porte do pet \"([^\"]*)\"$")
	public void informarOPorteDoPet(String porte) throws Throwable {

		pet.setShape(porte);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Porte do Pet: " + pet.getShape());
	}

	@Dado("^informar o peso em gramas \"([^\"]*)\"$")
	public void informarOPesoEmGramas(String pesoEmGramas) throws Throwable {

		pet.setWeightInGrams(pesoEmGramas);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Peso do Pet: " + pet.getWeightInGrams());
	}

	@Dado("^informar a url pet register$")
	public void informarAUrlPetRegister() throws Throwable {

		putUrl = URL_BASE + uuid + "/register/pet";
		System.out.println(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL Register: " + putUrl);
	}

	@Quando("^submeter a requisicao PUT de Pet$")
	public void submeterARequisicaoPutDePet() throws Throwable {

		String requestJson = gson.toJson(pet);

		System.out.println("Json criado: " + requestJson);
		response = request.body(requestJson).when().put(putUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL: " + putUrl);

	}

	// ******************************************************************UploadPetFile************************************************************************************//
	@Dado("^que informe o UUID \"([^\"]*)\"$")
	public void queInformeOUUID(String paramUuid) throws Throwable {

		uuid = paramUuid;

		ExtentCucumberFormatter.insertInfoTextInStepReport("Codigo UUID: " + uuid);
	}

	@Dado("^que o pet seja cadastrado \"([^\"]*)\"$")
	public void queOPetSejaCadastrado(String petId) throws Throwable {

		uploadFile = new UploadFile();
		uploadFile.setPetId(petId);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Pet id: " + uploadFile.getPetId());
	}

	@Dado("^informar o nome do arquivo \"([^\"]*)\"$")
	public void informarONomeDoArquivo(String nomeArquivo) throws Throwable {

		file = new File(System.getProperty("user.dir") + "/arquivos/" + nomeArquivo);
		uploadFile.setFile(file.getName());
		ExtentCucumberFormatter.insertInfoTextInStepReport("Nome do arquivo: " + nomeArquivo);
	}

	@Dado("^informar a url upload files$")
	public void informarAUrlUploadFiles() throws Throwable {

		postUrl = URL_BASE + uuid + "/register/pet/" + uploadFile.getPetId() + "/files";
		System.out.println(postUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL do upload files: " + postUrl);
	}

	@Quando("^submeter a requisicao POST de upload$")
	public void submeterARequisicaoPOSTDeUpload() throws Throwable {

		String requestJson = gson.toJson(uploadFile);
		response = given().header("country", "BR").multiPart(file).when().body(requestJson).post(postUrl);
	}

	@Então("^retornar os registros do upload com PetId \"([^\"]*)\" e o nome do arquivo \"([^\"]*)\"$")
	public void retornarOsRegistrosDoUploadComPetIdEONomeDoArquivo(String paramPetId, String nomeArquivo) throws Throwable {

		jPath = response.jsonPath();
		String petId = jPath.get("petId");
		String filename = jPath.get("filename");
		String nomeArquivoRetorno = petId + "-" + nomeArquivo;

		System.out.println("Dados de retorno: " + petId + " , " + filename);

		getRegisterJson = response.getBody().asString();
		System.out.println(getRegisterJson);
		ExtentCucumberFormatter.insertInfoTextInStepReport("Json de retorno: " + getRegisterJson);

		Assert.assertEquals("Pet ID invalido!", paramPetId, petId);
		Assert.assertEquals("Nome arquivo invalido!", nomeArquivoRetorno, filename);
	}

	@Então("^retornar a mensagem de erro do upload \"([^\"]*)\"$")
	public void retornarAMensagemDeErroDoUpload(String mensagem) throws Throwable {

		String message = response.getBody().asString();

		System.out.println(message);

		ExtentCucumberFormatter.insertInfoTextInStepReport("Mensagem de Erro: " + message);

		Assert.assertEquals("A mensagem: " + mensagem + " nao foi encontrada!", message.contains(mensagem), true);
	}

	@Dado("^informar a url upload files para exclusao \"([^\"]*)\"$")
	public void informarAUrlUploadFilesParaExclusao(String arquivo) throws Throwable {

		postUrl = URL_BASE + uuid + "/register/pet/" + uploadFile.getPetId() + "/files/" + arquivo;
		System.out.println(postUrl);
		ExtentCucumberFormatter.insertInfoTextInStepReport("URL do upload files exclusao: " + postUrl);

	}

	@Quando("^submeter a requisicao DELETE de upload$")
	public void submeterARequisicaoDELETEDeUpload() throws Throwable {

		String requestJson = gson.toJson(uploadFile);
		response = given().header("country", "BR").when().body(requestJson).delete(postUrl);
	}

}
