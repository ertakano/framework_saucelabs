package br.com.jungle.stepdefinitions;

import org.junit.Assert;

import com.github.javafaker.Faker;

import br.com.jungle.constants.UrlProperties;
import br.com.jungle.dao.Human;
import br.com.jungle.pageobjects.HumanPageObject;
import br.com.jungle.pageobjects.PreEmailPageObject;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class PreEmailSteps {
	
	private PreEmailPageObject preEmailPage = new PreEmailPageObject();
	private HumanPageObject humanPage = new HumanPageObject();
	private Human human = new Human();
	
	Faker faker = new Faker();
	String emailFaker = null;

	@Dado("^que eu esteja na home pre-email")
	public void que_eu_esteja_na_home_pre_email() throws Throwable {
		preEmailPage.navigateTo(UrlProperties.AWS_DEV_WEB);
		//Assert.assertTrue(preEmailPage.titleIsPresent());
	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^que o usuário não possua um e-mail cadastrado no sistema$")
	public void que_o_usuário_não_possua_um_e_mail_cadastrado_no_sistema() throws Throwable {
		emailFaker = faker.internet().emailAddress();
	}

	@Dado("^digitar o e-mail \"([^\"]*)\"$")
	public void digitar_o_e_mail(String email) throws Throwable {
				
		human.setEmail(email);
		
		if (human.getEmail().equals("")) {
			human.setEmail(emailFaker);
		}
		
		preEmailPage.setEmail(human.getEmail());
		
	}

	@Então("^deve visualizar a página para cadastrar os dados de Tutor$")
	public void deve_visualizar_a_página_para_cadastrar_os_dados_de_Tutor() throws Throwable {
		Assert.assertTrue(humanPage.titleIsPresent());
	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^que o usuário possua um e-mail com cadastro incompleto no sistema$")
	public void que_o_usuário_possua_um_e_mail_com_cadastro_incompleto_no_sistema() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^deve visualizar a página de Tutor com os dados previamente preenchidos$")
	public void deve_visualizar_a_página_de_Tutor_com_os_dados_previamente_preenchidos() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^que o usuário possua um e-mail cadastrado$")
	public void que_o_usuário_possua_um_e_mail_cadastrado() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^tenha realizado o cadastro de Tutor$")
	public void tenha_realizado_o_cadastro_de_Tutor() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^deve visualizar o campo de senha$")
	public void deve_visualizar_o_campo_de_senha() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^o link esqueci minha senha$")
	public void o_link_esqueci_minha_senha() throws Throwable {

	}

	@Dado("^que o usuário insira um e-mail cadastrado \"([^\"]*)\"$")
	public void que_o_usuário_insira_um_e_mail_cadastrado(String email) throws Throwable {
		preEmailPage.setEmail(email);
	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^insira uma senha cadastrada \"([^\"]*)\"$")
	public void insira_uma_senha_cadastrada(String arg1) throws Throwable {

	}

	@Quando("^submeter os dados$")
	public void submeter_os_dados() throws Throwable {
		preEmailPage.clickNextButton();
	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^deve visualizar a página de FAQ$")
	public void deve_visualizar_a_página_de_FAQ() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Quando("^clicar em esqueci minha senha$")
	public void clicar_em_esqueci_minha_senha() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^deve receber um e-mail para criar nova senha$")
	public void deve_receber_um_e_mail_para_criar_nova_senha() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^login deve ser bloqueado até a troca da senha$")
	public void login_deve_ser_bloqueado_até_a_troca_da_senha() throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^que o usuário insira um e-mail inválido$")
	public void que_o_usuário_insira_um_e_mail_inválido() throws Throwable {
		
	}
	
	/**
	 * TODO
	 * @throws Throwable
	 */
	@Dado("^que o usuário insira um e-mail em branco$")
	public void que_o_usuário_insira_um_e_mail_em_branco() throws Throwable {
		
		preEmailPage.setEmail("");
	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^deve visualizar a mensagem \"([^\"]*)\"$")
	public void deve_visualizar_a_mensagem(String arg1) throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Quando("^submeter a senha inválida \"([^\"]*)\"$")
	public void submeter_a_senha_inválida(String arg1) throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Quando("^submeter a senha em branco \"([^\"]*)\"$")
	public void submeter_a_senha_em_branco(String arg1) throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Quando("^submeter a senha com token expirado \"([^\"]*)\"$")
	public void submeter_a_senha_com_token_expirado(String arg1) throws Throwable {

	}

	/**
	 * TODO
	 * @throws Throwable
	 */
	@Então("^nao devo prosseguir para o proximo passo$")
	public void nao_devo_prosseguir_para_o_proximo_passo() throws Throwable {
		
		Assert.assertTrue(humanPage.titleIsPresent());
		//Assert.assertFalse(humanPage.titleIsPresent());
	}

	@Então("^visualizar o Nome \"([^\"]*)\"$")
	public void visualizar_o_Nome(String nome) throws Throwable {
		Assert.assertEquals(nome, humanPage.getFirstName());
	}

	@Então("^visualizar o Sobrenome \"([^\"]*)\"$")
	public void visualizar_o_Sobrenome(String sobrenome) throws Throwable {
		Assert.assertEquals(sobrenome, humanPage.getLastName());
	}

	@Então("^visualizar o E-mail \"([^\"]*)\"$")
	public void visualizar_o_E_mail(String email) throws Throwable {
		Assert.assertEquals(email, humanPage.getEmail());
	}

	@Então("^visualizar o CEP \"([^\"]*)\"$")
	public void visualizar_o_CEP(String cep) throws Throwable {
		Assert.assertEquals(cep, humanPage.getZipCode());
	}

	@Então("^visualizar o Celular \"([^\"]*)\"$")
	public void visualizar_o_Celular(String celular) throws Throwable {
		Assert.assertEquals(celular, humanPage.getCellPhone());
	}

	@Então("^visualizar Como Conheceu a Eleven Chimps \"([^\"]*)\"$")
	public void visualizar_Como_Conheceu_a_Eleven_Chimps(String comoConheceu) throws Throwable {
		
		Assert.assertEquals(comoConheceu, humanPage.getHowToKnowEC());
	}

	@Então("^visualizar Quantos Cachorros Tem \"([^\"]*)\"$")
	public void visualizar_Quantos_Cachorros_Tem(String qtosPets) throws Throwable {
		Assert.assertEquals(qtosPets, humanPage.getPetQuantity());
	}

	@Então("^visualizar os Nomes dos Cachorros \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
	public void visualizar_os_Nomes_dos_Cachorros(String dog5, String dog4, String dog3, String dog2, String dog1) throws Throwable {
		
		Assert.assertEquals(dog1, humanPage.getPetName(dog1));
		Assert.assertEquals(dog2, humanPage.getPetName(dog2));
		Assert.assertEquals(dog3, humanPage.getPetName(dog3));
		Assert.assertEquals(dog4, humanPage.getPetName(dog4));
		Assert.assertEquals(dog5, humanPage.getPetName(dog5));
	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo E-mail \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_email(String mensagem) throws Throwable {
		
		Assert.assertEquals(mensagem, preEmailPage.getEmailBlkMsg());
		
	}

}
