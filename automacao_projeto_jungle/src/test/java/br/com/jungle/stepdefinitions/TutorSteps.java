package br.com.jungle.stepdefinitions;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.github.javafaker.Faker;

import br.com.jungle.constants.UrlProperties;
import br.com.jungle.dao.Cellphone;
import br.com.jungle.dao.Human;
import br.com.jungle.pageobjects.PetPageObject;
import br.com.jungle.pageobjects.HumanPageObject;
import br.com.jungle.pageobjects.PreEmailPageObject;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class TutorSteps {

	private PreEmailPageObject preEmailPage = new PreEmailPageObject();
	private HumanPageObject humanPage = new HumanPageObject();
	private PetPageObject dogPage = new PetPageObject();
	private Human human = new Human();
	private Cellphone cellphone = new Cellphone();

	// Faker faker = new Faker(new Locale("pt-BR"));
	Faker fakerEmail = new Faker();
	int qtdOption = 0;

	@Dado("^que eu esteja na home do humano$")
	public void que_eu_esteja_na_home_do_humano() throws Throwable {
		preEmailPage.navigateTo(UrlProperties.AWS_DEV_WEB);
		//Assert.assertTrue(preEmailPage.titleIsPresent());
	}

	@Dado("^que o usuário logou no sistema com \"([^\"]*)\" e \"([^\"]*)\"$")
	public void que_o_usuário_logou_no_sistema_com_e(String email, String senha) throws Throwable {

		human.setEmail(email);
		
		if (human.getEmail().equals("")) {
			human.setEmail(fakerEmail.internet().emailAddress());
		}
		preEmailPage.setEmailAndClickNext(human.getEmail());
		Assert.assertTrue(humanPage.titleIsPresent());
	}

	@Dado("^que informou o campo Nome com letras,  acento agudo, circunflexo, til e cedilha \"([^\"]*)\"$")
	public void que_informou_o_campo_Nome_com_letras_acento_agudo_circunflexo_til_e_cedilha(String nome)
			throws Throwable {
		human.setName(nome);
		humanPage.setFirstName(human.getName());
	}

	@Dado("^informou o campo Nome \"([^\"]*)\"$")
	public void informou_o_campo_Nome(String nome) throws Throwable {
		human.setName(nome);
		humanPage.setFirstName(human.getName());
	}

	@Dado("^informou Sobrenome \"([^\"]*)\"$")
	public void informou_Sobrenome(String sobrenome) throws Throwable {
		human.setLastName(sobrenome);
		humanPage.setLastName(human.getLastName());
	}

	@Dado("^informou CEP \"([^\"]*)\"$")
	public void informou_CEP(String cep) throws Throwable {
		human.setZipCode(cep);
		humanPage.setZipCode(human.getZipCode());
	}

	@Dado("^informou Celular com DDD \"([^\"]*)\" e Numero \"([^\"]*)\"$")
	public void informou_Celular_com_ddd_e_numero(String ddd, String numero) throws Throwable {
		cellphone.setArea(ddd);
		cellphone.setNumber(numero);
		System.out.println(cellphone.getArea());
		System.out.println(cellphone.getNumber());
		
		human.setCellphone(cellphone);
		humanPage.setCellPhone(human.getCellphone().getArea()+human.getCellphone().getNumber());
	}

	@Dado("^informou Como conheceu a ElevenChimps \"([^\"]*)\"$")
	public void informou_Como_conheceu_a_ElevenChimps(String comoConheceu) throws Throwable {
		human.setHowToKnowEC(comoConheceu);
		humanPage.selectHowToKnowEC(human.getHowToKnowEC());
	}

	@Dado("^informou Quantos cachorros tem com o valor de \"([^\"]*)\"$")
	public void informou_Quantos_cachorros_tem_com_o_valor_de(String qtosCachorros) throws Throwable {
		human.setPetQuantity(qtosCachorros);
		humanPage.selectOptPetQuantity(human.getPetQuantity());
		qtdOption = Integer.parseInt(human.getPetQuantity());
	}

	@Dado("^informar os Nomes dos cachorros válidos$")
	public void informar_os_Nomes_dos_cachorros_válidos(List<String> petNames) throws Throwable {

		human.setPetNames(petNames);
		
		for (String petName : human.getPetNames()) {
			humanPage.setPetName(petName);
		}
	}

	@Dado("^informou o nome dos cachorros petUm \"([^\"]*)\",petDois \"([^\"]*)\",petTres \"([^\"]*)\",peQuatro \"([^\"]*)\",petCinco \"([^\"]*)\"$")
	public void informou_o_nome_dos_cachorros_petUm_petDois_petTres_peQuatro_petCinco(String pet1, String pet2,
			String pet3, String pet4, String pet5) throws Throwable {
		
		List<String> pets = new ArrayList<String>();
		pets.add(pet1);
		pets.add(pet2);
		pets.add(pet3);
		pets.add(pet4);
		pets.add(pet5);
		
		human.setPetNames(pets);
		humanPage.setFivePetNames(human.getPetNames().get(0), human.getPetNames().get(1), human.getPetNames().get(2), human.getPetNames().get(3), human.getPetNames().get(4));
	}

	@Então("^deve visualizar o cadastro de cachorros$")
	public void deve_visualizar_o_cadastro_de_cachorros() throws Throwable {
		Assert.assertTrue(dogPage.titleIsPresent());
	}

	@Quando("^submeter as informações$")
	public void submeter_as_informações() throws Throwable {
		humanPage.clickNextButton();
	}

	@Quando("^submeter as informações invalidas$")
	public void submeter_as_informações_invalidas() throws Throwable {
		humanPage.clickNextButton();
	}

	@Então("^deve visualizar uma mensagem de erro para o campo Nome \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_nome(String mensagem) throws Throwable {
		
		if(human.getName().equals("")) {
			Assert.assertEquals(mensagem, humanPage.getFirstNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}

	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo Sobrenome \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_Sobrenome(String mensagem) throws Throwable {

		if(human.getLastName().equals("")) {
			Assert.assertEquals(mensagem, humanPage.getLastNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}
	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo CEP \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_CEP(String mensagem) throws Throwable {
		
		if(human.getZipCode().equals("")) {
			Assert.assertEquals(mensagem, humanPage.getZipCodeBlkMsg());
		}else {
			Assert.assertEquals(mensagem, humanPage.getZipCodeBlkMsg());
		}

	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo Celular \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_Celular(String mensagem) throws Throwable {
		
		if(human.getCellphone().equals("")) {
			Assert.assertEquals(mensagem, humanPage.getCellPhoneBlkMsg());
		}else {
			Assert.assertEquals(mensagem, humanPage.getCellPhoneBlkMsg());
		}

	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo Como conheceu \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_Como_conheceu(String mensagem) throws Throwable {
		
		Assert.assertEquals(mensagem, humanPage.getHowToKnowECBlkMsg());
		
	}
	
	@Então("^deve visualizar uma mensagem de erro para o campo Nome do Cachorro \"([^\"]*)\"$")
	public void deve_visualizar_uma_mensagem_de_erro_para_o_campo_nome_do_cachorro(String mensagem) throws Throwable {
		
		if(human.getPetNames().get(0).equals("")) {
			Assert.assertEquals(mensagem, humanPage.getPetNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}

		if(human.getPetNames().get(1).equals("")) {
			Assert.assertEquals(mensagem, humanPage.getPetNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}

		if(human.getPetNames().get(2).equals("")) {
			Assert.assertEquals(mensagem, humanPage.getPetNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}

		if(human.getPetNames().get(3).equals("")) {
			Assert.assertEquals(mensagem, humanPage.getPetNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}
		
		if(human.getPetNames().get(4).equals("")) {
			Assert.assertEquals(mensagem, humanPage.getPetNameBlkMsg());
		}else {
			Assert.assertEquals(mensagem, "TODO");
		}
	}

	@Então("^não deve prosseguir com o cadastro$")
	public void não_deve_prosseguir_com_o_cadastro() throws Throwable {
		Assert.assertTrue(humanPage.titleIsPresent());
	}

	@Dado("^que informou CEP que não faz parte da região da entrega \"([^\"]*)\"$")
	public void que_informou_CEP_que_não_faz_parte_da_região_da_entrega(String cep) throws Throwable {
		human.setZipCode(cep);
		humanPage.setZipCode(human.getZipCode());
	}

	@Dado("^informou outros dados obrigatórios \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void informou_outros_dados_obrigatórios(String nome, String sobrenome, String ddd, String numero, String comoConheceu,
			String qtosDogs, String nomeDog) throws Throwable {
		humanPage.setFirstName(nome);
		humanPage.setLastName(sobrenome);
		cellphone.setArea(ddd);
		cellphone.setNumber(numero);
		humanPage.setCellPhone(cellphone.getArea()+cellphone.getNumber());
		humanPage.selectHowToKnowEC(comoConheceu);
		humanPage.selectOptPetQuantity(qtosDogs);
		humanPage.setPetName(nomeDog);
	}

	/**
	 * TODO
	 * 
	 * @throws Throwable
	 */
	@Então("^deve visualizar as informações que a região não faz parte da entrega$")
	public void deve_visualizar_as_informações_que_a_região_não_faz_parte_da_entrega() throws Throwable {

	}

	/**
	 * TODO
	 * 
	 * @throws Throwable
	 */
	@Então("^visualizar o cupom de desconto$")
	public void visualizar_o_cupom_de_desconto() throws Throwable {

	}

}
