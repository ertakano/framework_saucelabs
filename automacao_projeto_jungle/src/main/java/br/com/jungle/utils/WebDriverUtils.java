package br.com.jungle.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static br.com.jungle.core.DriverFactory.getDriver;

public class WebDriverUtils {

	private static long timeout = 10;
	public static WebDriverWait wait;

	public WebDriverUtils() {
		wait = new WebDriverWait(getDriver(), timeout);
	}

	/* TextField and TextArea */

	/**
	 * Method to clear and then type in the field or text area
	 * 
	 * @param by
	 * @param text
	 */
	public void setText(By by, String text) {
		clear(by);
		getDriver().findElement(by).sendKeys(text);
	}

	/**
	 * Method to clear fields and text area
	 * 
	 * @param by
	 */
	public void clear(By by) {
		waitElementToGoAppear(by);
		getDriver().findElement(by).clear();
	}

	/* Gets Atributes and Texts */

	/**
	 * Method to get the value by atribute of the tag
	 * 
	 * @param by
	 * @param attribute
	 * @return
	 */
	public String getAttributeTag(By by, String attribute) {
		return getDriver().findElement(by).getAttribute(attribute);
	}

	/**
	 * Method to get text
	 * 
	 * @param by
	 * @return
	 */
	public String getText(By by) {
		return getDriver().findElement(by).getText();
	}

	/* Verify Elements and Texts */

	/**
	 * Method to check if contains element
	 * 
	 * @param by
	 * @return
	 * @throws Exception
	 */
	public boolean containsElement(By by) throws Exception {
		try {
			waitElementToGoAppear(by);
			if (!getDriver().findElement(by).equals(null)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O elemento: " + getDriver().findElement(by) + " não foi encontrado", e);
		}
		return false;
	}

	/**
	 * Method to check if contains element by text on page
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public boolean containsElementsByText(String text) throws Exception {
		try {
			if (getDriver().getPageSource().contains(text)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O texto: " + text + " não foi encontrado", e);
		}
		return false;
	}

	/**
	 * Method to check if contains text in element
	 * 
	 * @param by
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public boolean containsTextInElement(By by, String text) throws Exception {
		try {
			waitElementToGoAppear(by);
			if (getDriver().findElement(by).getText().equals(text)) {
				return true;
			}
		} catch (Exception e) {
			throw new Exception("O texto: " + text + "não foi encontrado", e);
		}
		return false;
	}

	/* Waits */

	/**
	 * Method to wait driver
	 * 
	 * @return
	 */
	public WebDriverWait waitDriver() {
		return new WebDriverWait(getDriver(), 10);
	}

	/**
	 * Method to wait for the element to be visible
	 * 
	 * @param by
	 */
	public void waitElementToGoAppear(By by) {
		getDriver().manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		waitDriver().until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	/**
	 * Method to wait for the element to be invisible
	 * 
	 * @param by
	 */
	public void waitElementToGoDisappear(By by) {
		getDriver().manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
		waitDriver().until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	/**
	 * Method to wait loading page to be finished
	 * 
	 * @param by
	 */
	public void waitLoadingPageDisappear(By by) {
		waitElementToGoDisappear(by);
	}

	/* Frames and Windows */

	/**
	 * Method to enter the frame by id or name
	 * 
	 * @param idOrName
	 */
	public void enterToFrame(By by) {
		getDriver().switchTo().frame(getDriver().findElement(by));
	}

	/**
	 * Method to exit the frame and return to default content
	 */
	public void exitToFrame() {
		getDriver().switchTo().defaultContent();
	}

	/**
	 * Method to switch the window by id or name
	 * 
	 * @param idOrName
	 */
	public void changeWindow(String idOrName) {
		getDriver().switchTo().window(idOrName);
	}

	/**
	 * Method to switch to another open window
	 */
	public void changeWindow() {
		for (String winHandle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandle);
		}
	}

	/**
	 * Method to switch to before window
	 */
	public void beforeWindow() {
		String winHandleBefore = getDriver().getWindowHandle();
		getDriver().close();
		getDriver().switchTo().window(winHandleBefore);
	}

	/* Check is enable or disable */

	/**
	 * Method to check if element is enabled
	 * 
	 * @param by
	 * @return
	 */
	public boolean isEnabled(By by) {
		waitElementToGoAppear(by);
		return getDriver().findElement(by).isEnabled();
	}

	/**
	 * Method to check if element is displayed
	 * 
	 * @param by
	 * @return
	 */
	public boolean isDisplayed(By by) {
		waitElementToGoAppear(by);
		return getDriver().findElement(by).isDisplayed();
	}

	/**
	 * Method to find element By
	 * 
	 * @param by
	 * @return
	 */
	public WebElement findElement(By by) {
		return getDriver().findElement(by);
	}

	/**
	 * To GET state of checkbox
	 * 
	 * @param by
	 * @return boolean
	 */
	public boolean isCheckBoxSelected(By by) {
		return getDriver().findElement(by).isSelected();
	}

	/* Clicks */

	/**
	 * Method to click on element</br>
	 * Before click call <code>waitElementToGoAppear</code>
	 * 
	 * @param by
	 */
	public void click(By by) {
		waitElementToGoAppear(by);
		getDriver().findElement(by).click();
	}

	/**
	 * Method to click on button by name button
	 * 
	 * @param nameButton
	 */
	public void clickByNameButton(String nameButton) {
		click(By.xpath("//button[.='" + nameButton + "']"));
	}

	/**
	 * Method to move the focus to the desired element
	 * 
	 * @param by
	 */
	public void moveTo(By by) {
		WebElement element = getDriver().findElement(by);
		if ("input".equals(element.getTagName())) {
			element.sendKeys("");
		} else {
			new Actions(getDriver()).moveToElement(element).perform();
		}
	}

	/* Combos */

	/**
	 * Method to select item by visible text from combobox
	 * 
	 * @param by
	 * @param text
	 */
	public void selectItemByVisibleText(By by, String text) {
		waitElementToGoAppear(by);
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		combo.selectByVisibleText(text);
	}

	/**
	 * Method to select item by value from combobox
	 * 
	 * @param by
	 * @param value
	 */
	public void selectItemByValue(By by, String value) {
		waitElementToGoAppear(by);
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		combo.selectByValue(value);
	}
	
	/**
	 * Method to select text in value
	 * 
	 * @param by
	 * @param value
	 */
	public String getTextInValue(By by) {
		waitElementToGoAppear(by);
		WebElement element = getDriver().findElement(by);
		String text = element.getAttribute("value");
		return text;
	}

	/**
	 * Method to deselect item from combobox
	 * 
	 * @param by
	 * @param item
	 */
	public void deselectItem(By by, String item) {
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		combo.deselectByVisibleText(item);
	}

	/**
	 * Method to return item from combobox
	 * 
	 * @param by
	 * @return
	 */
	public String getSelectItem(By by) {
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		return combo.getFirstSelectedOption().getText();
	}

	/**
	 * Method to return itens from combobox
	 * 
	 * @param by
	 * @return
	 */
	public List<String> getSelectItens(By by) {
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		List<WebElement> allSelectedOptions = combo.getAllSelectedOptions();
		List<String> values = new ArrayList<String>();
		for (WebElement option : allSelectedOptions) {
			values.add(option.getText());
		}
		return values;
	}

	/**
	 * Method to return total count of itens from combobox
	 * 
	 * @param by
	 * @return
	 */
	public int getSelectCountTotalItens(By by) {
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		List<WebElement> options = combo.getOptions();
		return options.size();
	}

	/**
	 * Method to check if item exist on combobox
	 * 
	 * @param by
	 * @param item
	 * @return
	 */
	public boolean checkSelectItem(By by, String item) {
		WebElement element = getDriver().findElement(by);
		Select combo = new Select(element);
		List<WebElement> itens = combo.getOptions();
		for (WebElement itemList : itens) {
			if (itemList.getText().equals(item)) {
				return true;
			}
		}
		return false;
	}

	/* Browsers */

	/**
	 * Method to acess page by url
	 * 
	 * @param url
	 */
	public void navigateTo(String url) {
		getDriver().navigate().to(url);
	}

	/* Alerts */

	/**
	 * Method to accept window alert
	 */
	public void alertAccept() {
		Alert alert = getDriver().switchTo().alert();
		alert.accept();
	}

	/**
	 * Method to get text on the window alert
	 */
	public String alertGetText() {
		Alert alert = getDriver().switchTo().alert();
		return alert.getText();
	}

	/**
	 * Method to get text and accept window alert
	 * 
	 * @return
	 */
	public String alertGetTextAndAccept() {
		Alert alert = getDriver().switchTo().alert();
		String value = alert.getText();
		alert.accept();
		return value;

	}

	/**
	 * Method to get text and dismiss window alert
	 * 
	 * @return
	 */
	public String alertGetTextAndDismiss() {
		Alert alert = getDriver().switchTo().alert();
		String value = alert.getText();
		alert.dismiss();
		return value;

	}

	/* Tables */

	/**
	 * Method to search item in the table
	 * 
	 * @param searchItemTable
	 * @return By
	 */
	public By searchTableItem(String searchItemTable) {
		return By.xpath("//table//tr//td[contains(text(), '" + searchItemTable + "')]");
	}

	/**
	 * Method to get a cell by column name, row name, and selected column from a
	 * table
	 * 
	 * @param columnText
	 * @param lineText
	 * @param selectColumn
	 * @param byTable
	 * @return
	 */
	public WebElement getCellFromTable(String columnText, String lineText, String selectColumn, By byTable) {

		WebElement table = getDriver().findElement(byTable);
		int idColumn = getColumnIndex(columnText, table);

		int idLinha = getLineIndex(lineText, table, idColumn);

		int idSelectColumn = getColumnIndex(selectColumn, table);

		WebElement cell = table.findElement(By.xpath(".//tr[" + idLinha + "]/td[" + idSelectColumn + "]"));
		return cell;
	}

	/**
	 * Method to click button by column name, row name, and button column from a
	 * table
	 * 
	 * @param searchColumnText
	 * @param selectLineText
	 * @param buttonColumn
	 * @param byTable
	 */
	public void clickButtonTable(String searchColumnText, String selectLineText, String buttonColumn, By byTable) {
		WebElement cell = getCellFromTable(searchColumnText, selectLineText, buttonColumn, byTable);
		cell.findElement(By.xpath(".//input")).click();

	}

	/**
	 * Method to return line from a table
	 * 
	 * @param value
	 * @param table
	 * @param idColumn
	 * @return
	 */
	protected int getLineIndex(String value, WebElement table, int idColumn) {
		List<WebElement> lines = table.findElements(By.xpath("./tbody/tr/td[" + idColumn + "]"));
		int idLine = -1;
		for (int i = 0; i < lines.size(); i++) {
			if (lines.get(i).getText().equals(value)) {
				idLine = i + 1;
				break;
			}
		}
		return idLine;
	}

	/**
	 * Method to return column from a table
	 * 
	 * @param column
	 * @param table
	 * @return
	 */
	protected int getColumnIndex(String column, WebElement table) {
		List<WebElement> columns = table.findElements(By.xpath(".//th"));
		int idColumn = -1;
		for (int i = 0; i < columns.size(); i++) {
			if (columns.get(i).getText().equals(column)) {
				idColumn = i + 1;
				break;
			}
		}
		return idColumn;
	}

	/* Commons methods to use in PageObjects */

	/**
	 * Method to waiting loading page finished
	 */
	public void waitLoadingPage() {
		waitLoadingPageDisappear(By.xpath("/div[@class='loading ng-scope actived']"));
	}

	public void alterTab() {
		for (String winHandle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandle);
		}
	}

	public void returnAlterTab() {
		for (String winHandleBefore : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(winHandleBefore);
		}
	}

	public void selectElementListBox(By by, String itemValue) {
		waitElementToGoAppear(by);
		new Select(getDriver().findElement(by)).selectByVisibleText(itemValue);
	}
	
	/**
	 * Return instance WebDriver
	 * @return
	 */
	public WebDriver driver() {
		
		return getDriver();		
	}
	
	/**
	 * Return WebElement
	 * @param by
	 * @return
	 */
	public WebElement getWebElement(By by) {
		WebElement element = getDriver().findElement(by);
		return element;
	}

}
