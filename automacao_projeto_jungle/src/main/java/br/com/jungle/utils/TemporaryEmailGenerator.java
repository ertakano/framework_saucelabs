package br.com.jungle.utils;

import java.io.IOException;

import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import br.com.jungle.constants.UrlProperties;

public class TemporaryEmailGenerator {
	
	private static WebDriver driver = null;
	
	public String generateTemporaryEmail() throws ExecuteException, IOException, InterruptedException {
		
		System.out.println("inicio");
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		options.addArguments("window-size=1200x600");

		driver = new ChromeDriver(options);
		
		driver.navigate().to("https://temp-mail.org/pt/");
		driver.findElement(By.id("mail")).getAttribute("value");
		
		UrlProperties.EMAIL = driver.findElement(By.id("mail")).getAttribute("value");
		
		driver.quit();
		driver = null;
		
		return UrlProperties.EMAIL;
		
	}

}
