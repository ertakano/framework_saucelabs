package br.com.jungle.utils;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;

public class AppiumServerCommand {

	public static void startServer() throws ExecuteException, IOException, InterruptedException {

		CommandLine command = new CommandLine("/home/linuxbrew/.linuxbrew/Cellar/node/10.9.0/bin/node");
		command.addArgument("/home/linuxbrew/.linuxbrew/lib/node_modules/appium/build/lib/main.js", false);
		command.addArgument("--address", false);
		command.addArgument("127.1.1.1");
		command.addArgument("--port", false);
		command.addArgument("4723");
		//command.addArgument("--no-reset", false);
		command.addArgument("--full-reset", true);
		//command.addArgument("--session-override", true);
		//command.addArgument("--bootstrap-port", false);
		//command.addArgument("4724");

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);

		executor.execute(command, resultHandler);
		Thread.sleep(5000);
	}

	public static void stopServer() throws IOException {

		String[] command = { "/usr/bin/killall", "-KILL", "node" ,"&& kill -9 $(ps -A | grep -m1 Appium | awk '{print $1}')"};
		try {
			Runtime.getRuntime().exec(command);
			System.out.println("Appium server stopped...");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
