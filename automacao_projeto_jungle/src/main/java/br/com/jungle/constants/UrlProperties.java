package br.com.jungle.constants;

public class UrlProperties {

	// Site URL e-mail
	public static final String AWS_DEV_WEB = "http://localhost:8080/signup/email";
	public static final String AWS_DEV_MOBILE = "http://10.0.2.2:8080/signup/email";

	// Temporary e-mail address
	public static String EMAIL = null;

	// Sauce Labs URL
	public static final String USERNAME = "atakano";
	public static final String ACCESS_KEY = "d2e7a933-56c7-4493-a8e7-d4fefb1df60b";
	public static final String URL_SAUCE_LABS_WEB = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	public static final String URL_SAUCE_LABS_MOBILE = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";

	// Appium Server local
	public static final String URL_SERVER_APPIUM_LOCAL = "http://127.1.1.1:4723/wd/hub";
}