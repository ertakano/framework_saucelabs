package br.com.jungle.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadFile {
	
	private String petId;
	private String file;

}
