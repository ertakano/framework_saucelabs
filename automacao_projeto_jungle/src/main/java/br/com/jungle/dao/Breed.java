package br.com.jungle.dao;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Breed {
	
	private String adultIdealWeightInGrams;
	private String id;
	private String name;
	private List<RangesPhaseOfLive> rangesPhaseOfLive;
	private String sizeOfBreed;
	
	public Breed() {

	}
	
	public Breed(String id, String name) {
		this.setId(id);
		this.setName(name);
	}

	public Breed(String petName, RangesPhaseOfLive paramRangesPhaseOfLive) {
		
		this.setAdultIdealWeightInGrams("0");
		this.setId("string");
		this.setName(petName);
		
		rangesPhaseOfLive = new ArrayList<RangesPhaseOfLive>();
		rangesPhaseOfLive.add(paramRangesPhaseOfLive);
		this.setRangesPhaseOfLive(rangesPhaseOfLive);
		
		this.setSizeOfBreed("string");
	}

}
