package br.com.jungle.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LevelsOfGuarantee {
	
	private String description;
	private String percentage;

}
