package br.com.jungle.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cellphone {
	
	Cellphone cellphone;

	private String area;
	private String number;
	private String type;
	
	public Cellphone() {

		this.setArea("string");
		this.setNumber("string");
		this.setType("CELLPHONE");
		
	}

}
