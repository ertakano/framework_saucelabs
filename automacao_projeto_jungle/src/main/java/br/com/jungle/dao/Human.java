package br.com.jungle.dao;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Human {

	private Cellphone cellphone;
	private String email;
	private String howToKnowEC;
	private String lastName;
	private String name;
	private String uuid;
	private String country;

	private List<Pet> pets = new ArrayList<Pet>();

	private String status;
	private String zipCode;
	private String zipCodeCovered;

	private String petQuantity;
	private List<String> petNames;
	
}

