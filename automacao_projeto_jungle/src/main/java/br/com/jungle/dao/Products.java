package br.com.jungle.dao;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Products {
	
	private List<Attributes> attributes;
	private String caloriesPerGram;
	private String category;
	private List<String> countriesAvailable;
	private String description;
	private List<String> enrichments;
	private String id;
	private List<Ingredients> ingredients;
	private List<LevelsOfGuarantee> levelsOfGuarantee;
	private String name;
	private String petShape;
	private List<String> phasesOfLife;
	private Price price;
	private String principalCarbohydrate;
	private List<String> principalFeatures;
	private String principalProtein;
	private String sku;
	private String technicalDescription;
	private String typeProduct; 	

}
