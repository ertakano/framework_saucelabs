package br.com.jungle.dao;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HealthProblemsDetails {
	
	private String description;
	private List<String> problems;

	public HealthProblemsDetails(List<String> hProblems) {

		this.setDescription("string");
		this.setProblems(hProblems);
		
	}

}
