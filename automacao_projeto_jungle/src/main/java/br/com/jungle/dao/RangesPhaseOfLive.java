package br.com.jungle.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RangesPhaseOfLive {

	private String finalMonth;
	private String initialMonth;
	private String phaseOfLife;

	

}
