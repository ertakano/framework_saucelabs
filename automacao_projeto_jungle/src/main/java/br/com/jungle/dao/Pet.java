package br.com.jungle.dao;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Pet {

	private String ageInMonths;
	private String birthMonth;
	private Breed breed;
	private String castrated;
	private List<String> files;
	private String gender;
	private String healthProblems;
	private HealthProblemsDetails healthProblemsDetails;
	private String id;
	private String idealWeightInGrams;
	private String lastName;
	private String mealsPerDay;
	private String name;
	private List<RangesPhaseOfLive> rangesPhaseOfLive;
	private String preferredProtein;
	private String shape;
	private String status;
	private String weightInGrams;
	
	public Pet(String id, String name) {
		
		this.setId(id);
		this.setName(name);
		
	}
	
}
