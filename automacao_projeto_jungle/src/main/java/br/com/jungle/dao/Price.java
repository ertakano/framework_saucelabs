package br.com.jungle.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Price {

	private String listPriceInCents;
	private String salePriceInCents;
}
