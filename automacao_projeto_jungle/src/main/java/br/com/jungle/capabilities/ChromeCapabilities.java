package br.com.jungle.capabilities;

import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeCapabilities {
	
	public static DesiredCapabilities capabilitiesLocal() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
		 DesiredCapabilities capabilities = DesiredCapabilities.chrome();

		
		return capabilities;
		
	}
	
	public static DesiredCapabilities windows_10_Chrome_Version_68_0() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
	    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	    capabilities.setCapability("platform", "Windows 10");
	    capabilities.setCapability("version", "68.0");
		return capabilities;
		
	}


}
