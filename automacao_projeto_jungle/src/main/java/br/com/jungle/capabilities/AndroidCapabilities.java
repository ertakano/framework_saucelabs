package br.com.jungle.capabilities;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class AndroidCapabilities {
	
	public static DesiredCapabilities capabilitiesLocal() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
		DesiredCapabilities capabilities = DesiredCapabilities.android();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");

		// --nao fazer o reset do browser
		// capabilities.setCapability("noReset", "true");
		// capabilities.setCapability("fullReset", "false");

		// --Capacidade para executar com Dispositivo Fisico
		//capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Moto_G__5S_");
		//capabilities.setCapability(MobileCapabilityType.UDID, "0036371198");

		// --Capacidade para executar com Emulador
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
		capabilities.setCapability(MobileCapabilityType.UDID, "emulator-5554");
		capabilities.setCapability("avd", "Nexus_5X_API_28");
		
		return capabilities;
		
	}
	
	public static DesiredCapabilities sansung_Galaxy_S4_PlatformVersion_4_4() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
	    DesiredCapabilities capabilities = DesiredCapabilities.android();
	    capabilities.setCapability("platformName", "Android");
	    capabilities.setCapability("deviceName", "Samsung Galaxy S4 Emulator");
	    capabilities.setCapability("platformVersion", "4.4");
	    //capabilities.setCapability("app", "http://saucelabs.com/example_files/ContactManager.apk");
	    capabilities.setCapability("browserName", "Browser");
	    capabilities.setCapability("deviceOrientation", "portrait");
	    capabilities.setCapability("appiumVersion", "1.5.3");
		
		return capabilities;
		
	}


}
