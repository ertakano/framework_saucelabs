package br.com.jungle.capabilities;

import org.openqa.selenium.remote.DesiredCapabilities;

public class IOSCapabilities {
	
	public static DesiredCapabilities capabilitiesLocal() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
        DesiredCapabilities capabilities = DesiredCapabilities.iphone();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone 6");
        capabilities.setCapability("platformVersion", "8.4");
        //capabilities.setCapability("app", "https://s3.amazonaws.com/appium/TestApp8.4.app.zip");
        capabilities.setCapability("browserName", "");
        capabilities.setCapability("deviceOrientation", "portrait");
        capabilities.setCapability("appiumVersion", "1.5.3");
		
		return capabilities;
		
	}
	
	public static DesiredCapabilities iphone_6_Platform_Version_11_0() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
		DesiredCapabilities capabilities = DesiredCapabilities.iphone();
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone 6 Simulator");
        capabilities.setCapability("platformVersion", "11.0");
        //capabilities.setCapability("app", "https://s3.amazonaws.com/appium/TestApp8.4.app.zip");
        capabilities.setCapability("browserName", "Safari");
        capabilities.setCapability("deviceOrientation", "portrait");
        capabilities.setCapability("appiumVersion", "1.8.1");
		
		return capabilities;
		
	}

}
