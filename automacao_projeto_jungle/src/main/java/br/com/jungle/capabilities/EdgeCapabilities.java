package br.com.jungle.capabilities;

import org.openqa.selenium.remote.DesiredCapabilities;

public class EdgeCapabilities {
	
	public static DesiredCapabilities windows_10_Edge_Version_16_16299() {
		
		// --capacidades indicando a plataforma, dispositivo e aplicacao
	    DesiredCapabilities capabilities = DesiredCapabilities.edge();
	    capabilities.setCapability("platform", "Windows 10");
	    capabilities.setCapability("version", "16.16299");
		return capabilities;
		
	}


}
