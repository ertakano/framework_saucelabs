package br.com.jungle.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import br.com.jungle.dao.Human;
import br.com.jungle.utils.WebDriverUtils;

public class HumanPageObject extends WebDriverUtils {

	private By titleHumanPage = By.xpath("//h1[text()='Parabéns pela decisão!']");
	private By fldFirstName = By.id("name");
	private By fldLastName = By.id("lastName");
	private By fldEmail = By.id("email");
	private By fldZipCode = By.id("zipCode");
	private By fldCellPhone = By.id("cellphone");
	private By selHowToKnowEC = By.id("howToKnowEC");
	private By fldPetQuantity = By.name("petQuantity");

	private By fldFirstNameBlkMsg = By.xpath("//input[@id='name']/../span");
	private By fldLastNameBlkMsg = By.xpath("//input[@id='lastName']/../span");
	private By fldZipCodeBlkMsg = By.xpath("//input[@id='zipCode']/../span");
	private By fldCellPhoneBlkMsg = By.xpath("//input[@id='cellphone']/../span");
	private By selHowToKnowECBlkMsg = By.xpath("//select[@id='howToKnowEC']/../span");
	private By fldPetNameBlkMsg = By.xpath("//input[@id='petName']/../span");	

	private By fldPetNameOne = By.id("petName");
	private By fldPetNametwo = By.id("petName1");
	private By fldPetNamethree = By.id("petName2");
	private By fldPetNamefour = By.id("petName3");
	private By fldPetNameFive = By.id("petName4");

	String varPetName = null;
	private By fldPetName;
	private By optPetQuantity;

	private By btnProximoPasso = By.xpath("//button[text()='Próximo passo']");

	String option = null;
	int idPetName = 0;

	public void setFirstName(String firstName) {
		click(fldFirstName);
		clear(fldFirstName);
		setText(fldFirstName, firstName);
	}

	public void setLastName(String lastName) {
		click(fldLastName);
		clear(fldLastName);
		setText(fldLastName, lastName);
	}

	public String getEmail() {
		return getAttributeTag(fldEmail, "value");
	}

	public void setZipCode(String zipCode) {
		click(fldZipCode);
		clear(fldZipCode);
		setText(fldZipCode, zipCode);
	}

	public void setCellPhone(String cellPhone) {
		click(fldCellPhone);
		clear(fldCellPhone);
		setText(fldCellPhone, cellPhone);
	}

	public void selectHowToKnowEC(String value) {
		selectItemByValue(selHowToKnowEC, value);
	}

	public void setPetName(String petNames) {

		if (idPetName == 0) {
			fldPetName = By.id("petName");
			click(fldPetName);
			clear(fldPetName);
			setText(fldPetName, petNames);

		} else {
			fldPetName = By.id("petName" + idPetName);
			click(fldPetName);
			clear(fldPetName);
			setText(fldPetName, petNames);

			idPetName = idPetName - 1;
		}
	}

	public void setFivePetNames(String pet1, String pet2, String pet3, String pet4, String pet5) {

		click(fldPetNameOne);
		clear(fldPetNameOne);
		setText(fldPetNameOne, pet1);

		click(fldPetNametwo);
		clear(fldPetNametwo);
		setText(fldPetNametwo, pet2);

		click(fldPetNamethree);
		clear(fldPetNamethree);
		setText(fldPetNamethree, pet3);

		click(fldPetNamefour);
		clear(fldPetNamefour);
		setText(fldPetNamefour, pet4);

		click(fldPetNameFive);
		clear(fldPetNameFive);
		setText(fldPetNameFive, pet5);

	}

	public void selectOptPetQuantity(String option) {
		optPetQuantity = By.xpath("//input[@name='petQuantity']/../ul/li[@data-value='" + option + "']");
		click(optPetQuantity);
		this.option = option;
		idPetName = Integer.parseInt(option) - 1;
	}

	public boolean titleIsPresent() {
		try {
			return containsElement(titleHumanPage);
		} catch (Exception e) {
			System.out.println("Titulo: Parabéns pela decisão!, nao esta presente.");
			e.printStackTrace();
		}
		return true;
	}

	public void clickNextButton() {
		click(btnProximoPasso);
	}

	/**
	 * Blank messages
	 * @return
	 */
	public String getFirstNameBlkMsg() {
		return getText(fldFirstNameBlkMsg);
	}

	public String getLastNameBlkMsg() {
		return getText(fldLastNameBlkMsg);
	}

	public String getZipCodeBlkMsg() {
		return getText(fldZipCodeBlkMsg);
	}

	public String getCellPhoneBlkMsg() {
		return getText(fldCellPhoneBlkMsg);
	}

	public String getHowToKnowECBlkMsg() {
		return getText(selHowToKnowECBlkMsg);
	}

	public String getPetNameBlkMsg() {
		return getText(fldPetNameBlkMsg);
	}
	
	/**
	 * Get Text of Value
	 * @return
	 */
	public String getFirstName() {
		return getAttributeTag(fldFirstName, "value");
	}

	public String getLastName() {
		return getAttributeTag(fldLastName, "value");
	}

	public String getZipCode() {
		return getAttributeTag(fldZipCode, "value");
	}

	public String getCellPhone() {
		return getAttributeTag(fldCellPhone, "value");
	}

	public String getHowToKnowEC() throws Exception {

		for(int i=1; i<7; i++) {
			By option = By.xpath("//*[@id='howToKnowEC']/option["+i+"]");
			WebElement optionHowToKnowEC = findElement(option);
			
			if(optionHowToKnowEC.isSelected()) {
				return getText(option);
			};
		}
		return null;
	}
	
	public String getPetQuantity() {
		return getAttributeTag(fldPetQuantity, "value");
	}

	public String getPetNameOne() {
		return getAttributeTag(fldPetNameOne, "value");
	}
	
	public String getPetNameTwo() {
		return getAttributeTag(fldPetNametwo, "value");
	}
	
	public String getPetNameThree() {
		return getAttributeTag(fldPetNamethree, "value");
	}
	
	public String getPetNameFour() {
		return getAttributeTag(fldPetNamefour, "value");
	}
	
	public String getPetNameFive() {
		return getAttributeTag(fldPetNameFive, "value");
	}
	
	public String getPetName(String petName) {
		
		varPetName = getAttributeTag(By.xpath("//input[@id='petName']"), "value");
		
		if(varPetName.equals(petName)) {
			return varPetName;
		}else {		
			for(int i=1; i<5; i++) {
				varPetName = getAttributeTag(By.xpath("//input[@id='petName"+i+"']"), "value");
					if(varPetName.equals(petName)) {
						return varPetName;
					}
				}
		}
		return null;
	}

	public void registerHuman(Human humanPage) {

//		setFirstName(humanPage.getFirstName());
//		setLastName(humanPage.getLastName());
//		setZipCode(humanPage.getZipCode());
//		setCellPhone(humanPage.getCellPhone());
//		selectHowToKnowEC(humanPage.getHowToKnowEC());
//		selectOptPetQuantity(humanPage.getPetQuantity());

		for (String petName : humanPage.getPetNames()) {
			setPetName(petName);
		}

		clickNextButton();
	}

}
