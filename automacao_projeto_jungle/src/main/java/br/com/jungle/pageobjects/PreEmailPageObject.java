package br.com.jungle.pageobjects;

import org.openqa.selenium.By;

import br.com.jungle.utils.WebDriverUtils;

public class PreEmailPageObject extends WebDriverUtils{

	private By titlePreEmailPage = By.xpath("//h1[text()='Vamos começar pelo seu e-mail']");
	private By fldEmail = By.id("email");
	private By clickNextButton = By.xpath("//button[text()='Próximo passo']");
	private By fldPetNameBlkMsg = By.xpath("//input[@id='email']/../span");
		
	public void setEmail(String email) {
		click(fldEmail);
		clear(fldEmail);
		setText(fldEmail, email);
	}
	
	public void clickNextButton() {
		click(clickNextButton);
	}
		
	public By getTitlePreEmailPage() {
		return titlePreEmailPage;
	}
	
	public boolean titleIsPresent() {
		try {
			containsElement(titlePreEmailPage);
		} catch (Exception e) {
			System.out.println("Titulo: Vamos começar pelo seu e-mail, nao esta presente.");
			e.printStackTrace();
		}
		return true;
	}
	
	public String getEmailBlkMsg() {
		return getText(fldPetNameBlkMsg);
	}
	
	public void setEmailAndClickNext(String email) {		
		setEmail(email);
		clickNextButton();
	}

}
