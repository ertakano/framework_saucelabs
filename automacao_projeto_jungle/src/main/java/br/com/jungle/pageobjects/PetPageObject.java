package br.com.jungle.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import br.com.jungle.utils.WebDriverUtils;

public class PetPageObject extends WebDriverUtils{

	private By titleDogPage = By.xpath("//h1[contains(.,'Agora sobre:')]");
	private By btnGenderMale = By.xpath("//input[@value = 'M']/../span");
	private By btnGenderFemale = By.xpath("//input[@value = 'F']/../span");
	private By fldBirthMonth = By.id("birthMonth");
	private By fldBreedName = By.id("breed-search");
	private By selWeightKG = By.id("weight-kg");
	private By selWeightGR = By.id("weight-g");
	private By btnMealsPerDay;
	private By btnShapeThin = By.xpath("//input[@id='flat']/../label");
	private By btnShapeNormal = By.xpath("//input[@id='normal']/../label");
	private By btnShapeFat = By.xpath("//input[@id='fat']/../label");
	private By btnShapePlusSize = By.xpath("//input[@id='plus-size']/../label");
	private By selIdealWeightKG = By.id("ideal-weight-kg");
	private By selIdealWeightGR = By.id("ideal-weight-g");
	private By radioBeef = By.xpath("//input[@id='preferred-protein-beef']/../label");
	private By radioChicken = By.xpath("//input[@id='preferred-protein-chicken']/../label");
	private By radioWhatever = By.xpath("//input[@id='preferred-protein-whatever']/../label");
	private By radioCastratedYes = By.xpath("//input[@id='castrated-yes']/../label");
	private By radioCastratedNo = By.xpath("//input[@id='castrated-no']/../label");
	private By radioHealthProblemsYes = By.xpath("//input[@id='health-problems-yes']/../label");
	private By radioHealthProblemsNo = By.xpath("//input[@id='health-problems-no']/../label");
	private By cbAlergyChecked = By.xpath("//input[@id='other-health-problems-alergy']");
	private By cbFoodAlergyChecked = By.xpath("//input[@id='other-health-problems-food-allergy']");
	private By cbCardiologyChecked = By.xpath("//input[@id='other-health-problems-cardiology']");
	private By cbDiabetesChecked = By.xpath("//input[@id='other-health-problems-diabetes']");
	private By cbGastrointestinalChecked = By.xpath("//input[@id='other-health-problems-gastrointestinal']");
	private By cbRenalChecked = By.xpath("//input[@id='other-health-problems-renal']");
	private By cbOtherChecked = By.xpath("//input[@id='other-health-problems-other']");	
	private By cbAlergy = By.xpath("//input[@id='other-health-problems-alergy']/../label");
	private By cbFoodAlergy = By.xpath("//input[@id='other-health-problems-food-allergy']/../label");
	private By cbCardiology = By.xpath("//input[@id='other-health-problems-cardiology']/../label");
	private By cbDiabetes = By.xpath("//input[@id='other-health-problems-diabetes']/../label");
	private By cbGastrointestinal = By.xpath("//input[@id='other-health-problems-gastrointestinal']/../label");
	private By cbRenal = By.xpath("//input[@id='other-health-problems-renal']/../label");
	private By cbOther = By.xpath("//input[@id='other-health-problems-other']/../label");
	private By txtDescription = By.id("healthProblemsDescription");
	private By fldUpload = By.id("upload");
	private By btnNext = By.xpath("//button[text()='Próximo passo']");
	
	List<String> listProblems;
	
	public boolean titleIsPresent() {
		try {
			return containsElement(titleDogPage);
		} catch (Exception e) {
			System.out.println("Titulo: Agora sobre:, nao esta presente.");
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean selectGender(String option) {
		
		if(option.equals("Macho")) {
			click(btnGenderMale);			
		}else if(option.equals("Femea")) {
			click(btnGenderFemale);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Macho ou Femea");
			return false;
		}
		return true;
	}
	
	public void setBirthMonth(String birthMonth) {
		click(fldBirthMonth);
		clear(fldBirthMonth);
		setText(fldBirthMonth, birthMonth);
	}
	
	public void setBreedName(String breedName) throws InterruptedException {
		click(fldBreedName);
		clear(fldBreedName);
		   
		String threeFirstDig = breedName.substring(0, 3);
		
		setText(fldBreedName, threeFirstDig);
		Thread.sleep(500);	
		
		selectBreedNameOnList(breedName);
	}
	
	public String getBreedName() {
		
		JavascriptExecutor js = (JavascriptExecutor) driver();
		String breedName=(String) js.executeScript("return document.getElementById(\"breed\").getAttribute(\"value\");");
		System.out.println("raca: " + breedName);
		
		return breedName;
	}
	
	public void selectBreedNameOnList(String breedName) {
		By searchBreedName = By.xpath("//ul/li[@data-name='"+breedName+"']");
		click(searchBreedName);
	}
		
	public void selectWeightKG(String value) {
		selectItemByValue(selWeightKG, value);
	}
	
	public void selectWeightGR(String value) {
		selectItemByValue(selWeightGR, value);
	}
	
	public void clicktMealsPerDay(String value) {
		btnMealsPerDay = By.xpath("//input[@name='mealsPerDay']/../ul/li[text()='"+value+"']");
		click(btnMealsPerDay);
	}
	
	public boolean selectShape(String shape) {
		
		if(shape.equals("Magro")) {
			click(btnShapeThin);
		}
		else if(shape.equals("Normal")) {
			click(btnShapeNormal);
		}
		else if(shape.equals("Gordinho")) {
			click(btnShapeFat);
		}
		else if(shape.equals("Plus size")) {
			click(btnShapePlusSize);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Magro, Normal, Gordinho ou Plus size");
			return false;
		}
		return true;
	}
	
	public void selectIdealWeightKG(String value) {
		selectItemByValue(selIdealWeightKG, value);
	}
	
	public void selectIdealWeightGR(String value) {
		selectItemByValue(selIdealWeightGR, value);
	}
	
	public boolean clickPreferedProtein(String protein) {
		
		if(protein.equals("Carne")) {
			click(radioBeef);
		}
		else if(protein.equals("Frango")) {
			click(radioChicken);
		}
		else if(protein.equals("Tanto faz")) {
			click(radioWhatever);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Carne, Frango ou Tanto faz");
			return false;
		}
		return true;
	}
	
	public boolean isCastrated(String option) {
		
		if(option.equals("sim") ) {
			click(radioCastratedYes);
		}
		else if(option.equals("nao")){
			click(radioCastratedNo);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: sim ou nao");
			return false;
		}
		return true;
	}
	
	public boolean hasHealthProblems(String option) {
		
		if(option.equals("sim") ) {
			click(radioHealthProblemsYes);
		}
		else if(option.equals("nao")){
			click(radioHealthProblemsNo);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: sim ou nao");
			return false;
		}	
		return true;
	}
	
	public boolean selectProblems(List<String> problems) {
				
		listProblems = new ArrayList<String>();
		
		listProblems.addAll(problems);
		
		if(listProblems.contains("Alergia")) {
			if(!isCheckBoxSelected(cbAlergyChecked)) {
				click(cbAlergy);
			}			
		}
		if(listProblems.contains("Alergia alimentar")){
			if(!isCheckBoxSelected(cbFoodAlergyChecked)) {
				click(cbFoodAlergy);
			}			
		}
		if(listProblems.contains("Cardiologico")){
			if(!isCheckBoxSelected(cbCardiologyChecked)) {
				click(cbCardiology);
			}			
		}
		if(listProblems.contains("Diabetes")){
			if(!isCheckBoxSelected(cbDiabetesChecked)) {
				click(cbDiabetes);
			}			
		}
		if(listProblems.contains("Gastrointestinal")){
			if(!isCheckBoxSelected(cbGastrointestinalChecked)) {
				click(cbGastrointestinal);
			}
		}
		if(listProblems.contains("Renal")){
			if(!isCheckBoxSelected(cbRenalChecked)) {
				click(cbRenal);
			}
		}
		if(listProblems.contains("Outro")){
			if(!isCheckBoxSelected(cbOtherChecked)) {
				click(cbOther);
			}
		}
		else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Alergia, Alergia alimentar, Cardiologico, Diabetes, Gastrointestinal, Renal ou Outro");
			return false;
		}		
		return true;
	}
	
	public void setDescription(String description) {
		click(txtDescription);
		clear(txtDescription);
		setText(txtDescription, description);
	}

	public void clickNextButton() {
		click(btnNext);
	}
	
}
