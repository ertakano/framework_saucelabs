package br.com.jungle.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;

import br.com.jungle.utils.WebDriverUtils;

public class DogPageObject extends WebDriverUtils{

	private By titleDogPage = By.xpath("//h1[contains(.,'Agora sobre:')]");
	private By btnGenderMale = By.xpath("//input[@value = 'M']");
	private By btnGenderFemale = By.xpath("//input[@value = 'F']");
	private By fldBirthMonth = By.id("birthMonth");
	private By fldBreedName = By.id("breed-search");
	private By selWeightKG = By.id("weight-kg");
	private By selWeightGR = By.id("weight-g");
	private By btnMealsPerDay;
	private By btnShapeThin = By.xpath("//input[@id='flat']/../label");
	private By btnShapeNormal = By.xpath("//input[@id='normal']/../label");
	private By btnShapeFat = By.xpath("//input[@id='fat']/../label");
	private By btnShapePlusSize = By.xpath("//input[@id='plus-size']/../label");
	private By selIdealWeightKG = By.id("ideal-weight-kg");
	private By selIdealWeightGR = By.id("ideal-weight-g");
	private By radioBeef = By.id("preferred-protein-beef");
	private By radioChicken = By.id("preferred-protein-chicken");
	private By radioWhatever = By.id("preferred-protein-whatever");
	private By radioCastratedYes = By.id("castrated-yes");
	private By radioCastratedNo = By.id("castrated-no");
	private By radioHealthProblemsYes = By.id("health-problems-yes");
	private By radioHealthProblemsNo = By.id("health-problems-no");
	private By cbAlergy = By.id("other-health-problems-alergy");
	private By cbFoodAlergy = By.id("other-health-problems-food-allergy");
	private By cbCardiology = By.id("other-health-problems-cardiology");
	private By cbDiabetes = By.id("other-health-problems-diabetes");
	private By cbGastrointestinal = By.id("other-health-problems-gastrointestinal");
	private By cbRenal = By.id("other-health-problems-renal");
	private By cbOther = By.id("other-health-problems-other");
	private By txtDescription = By.id("healthProblemsDescription");
	private By fldUpload = By.id("upload");
	private By btnNext = By.xpath("//button[text()='Próximo passo']");
	
	List<String> listProblems;
	
	public boolean titleIsPresent() {
		try {
			return containsElement(titleDogPage);
		} catch (Exception e) {
			System.out.println("Titulo: Agora sobre:, nao esta presente.");
			e.printStackTrace();
		}
		return true;
	}
	
	public void selectGender(String option) {
		
		if(option.equals("Macho")) {
			click(btnGenderMale);
		}
		if(option.equals("Femea")) {
			click(btnGenderFemale);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Macho ou Femea");
		}
	}
	
	public void setBirthMonth(String birthMonth) {
		click(fldBirthMonth);
		clear(fldBirthMonth);
		setText(fldBirthMonth, birthMonth);
	}
	
	public void setBreedName(String breedName) {
		click(fldBreedName);
		clear(fldBreedName);
		setText(fldBreedName, breedName);
	}
	
	public void selectWeightKG(String value) {
		selectItemByValue(selWeightKG, value);
	}
	
	public void selectWeightGR(String value) {
		selectItemByValue(selWeightGR, value);
	}
	
	public void clicktMealsPerDay(String value) {
		btnMealsPerDay = By.xpath("//input[@name='mealsPerDay']/../ul/li[text()='"+value+"']");
		click(btnMealsPerDay);
	}
	
	public void selectShape(String shape) {
		
		if(shape.equals("Magro")) {
			click(btnShapeThin);
		}
		if(shape.equals("Normal")) {
			click(btnShapeNormal);
		}
		if(shape.equals("Gordinho")) {
			click(btnShapeFat);
		}
		if(shape.equals("Plus size")) {
			click(btnShapePlusSize);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Magro, Normal, Gordinho ou Plus size");
		}
	}
	
	public void selectIdealWeightKG(String value) {
		selectItemByValue(selIdealWeightKG, value);
	}
	
	public void selectIdealWeightGR(String value) {
		selectItemByValue(selIdealWeightGR, value);
	}
	
	public void clickPreferedProtein(String protein) {
		
		if(protein.equals("Carne")) {
			click(radioBeef);
		}
		if(protein.equals("Frango")) {
			click(radioChicken);
		}
		if(protein.equals("Tanto faz")) {
			click(radioWhatever);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Carne, Frango ou Tanto faz");
		}
	}
	
	public void isCastrated(String option) {
		
		if(option.equals("sim") ) {
			click(radioCastratedYes);
		}
		if(option.equals("nao")){
			click(radioCastratedNo);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: sim ou nao");
		}		
	}
	
	public void hasHealthProblems(String option) {
		
		if(option.equals("sim") ) {
			click(radioHealthProblemsYes);
		}
		if(option.equals("nao")){
			click(radioHealthProblemsNo);
		}else {
			System.out.println("Opcao nao existe! Escolha as opcoes: sim ou nao");
		}		
	}
	
	public void selectProblems(List<String> problems) {
		
		listProblems = new ArrayList<String>();
		
		if(listProblems.contains("Alergia") ) {
			click(cbAlergy);
		}
		if(listProblems.contains("Alergia alimentar")){
			click(cbFoodAlergy);
		}
		if(listProblems.contains("Cardiologico")){
			click(cbCardiology);
		}
		if(listProblems.contains("Diabetes")){
			click(cbDiabetes);
		}
		if(listProblems.contains("Gastrointestinal")){
			click(cbGastrointestinal);
		}
		if(listProblems.contains("Renal")){
			click(cbRenal);
		}
		if(listProblems.contains("Outro")){
			click(cbOther);
		}
		else {
			System.out.println("Opcao nao existe! Escolha as opcoes: Alergia, Alergia alimentar, Cardiologico, Diabetes, Gastrointestinal, Renal ou Outro");
		}			
	}
	
	public void setDescription(String description) {
		click(txtDescription);
		clear(txtDescription);
		setText(txtDescription, description);
	}
	
	public void clickNextButton() {
		click(btnNext);
	}
	
}
