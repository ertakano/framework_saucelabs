package br.com.jungle.core;

import java.io.IOException;

import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.remote.DesiredCapabilities;

import br.com.jungle.utils.AppiumServerCommand;

public class Properties {
	
	/**
	 * choose the execution type 
	 */
	public static ExecutionType EXECUTION_TYPE = null;
	
	/**
	 * choose the browsers or mobile system 
	 */
	public static SystemConfig SYSTEM_CONFIG = null;
	
	/**
	 * choose the Capabilities
	 */
	public static DesiredCapabilities CAPABILITIES = null;
	
	public enum SystemConfig {
		CHROME,
		FIREFOX,
		CHROMEHEADLESS,
		EDGE,
		ANDROID,
		IOS
	}
	
	public enum ExecutionType {
		LOCAL,
		SAUCELABS
	}
	
	/**
	 * 
	 * @param executionType
	 * @param systemConfig
	 * @param systemMobile It's mandatory only use "executionType.LOCAL_MOBILE"
	 */
	public static void setProperties(ExecutionType executionType, SystemConfig systemConfig, DesiredCapabilities setCapabilities) {
		
		EXECUTION_TYPE = executionType;
		SYSTEM_CONFIG = systemConfig;
		CAPABILITIES = setCapabilities;
		
		if(executionType.equals(EXECUTION_TYPE.LOCAL)) {
			
			if(systemConfig.equals(SYSTEM_CONFIG.ANDROID) || systemConfig.equals(SYSTEM_CONFIG.IOS)) {
				try {
					AppiumServerCommand.stopServer();
					AppiumServerCommand.startServer();
				} catch (ExecuteException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
			
		} 
		
	}

}
