package br.com.jungle.core;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import br.com.jungle.constants.UrlProperties;
import br.com.jungle.core.Properties.ExecutionType;
import br.com.jungle.utils.AppiumServerCommand;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class DriverFactory {
	
	private static ThreadLocal<WebDriver> threadDriver = new ThreadLocal<WebDriver>(){
		@Override
		protected synchronized WebDriver initialValue(){
			return createDriver();
		}
	};
	
	private DriverFactory() {}

	public static WebDriver createDriver() {

		WebDriver driver = null;
		
		if (Properties.EXECUTION_TYPE == ExecutionType.LOCAL) {

			switch (Properties.SYSTEM_CONFIG) {

			case FIREFOX:
				if (driver == null) {
					System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver");
					driver = new FirefoxDriver();
				}
				break;

			case CHROME:
				if (driver == null) {
					System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
					driver = new ChromeDriver();
				}
				break;

			case CHROMEHEADLESS:
				if (driver == null) {

					System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("headless");
					options.addArguments("window-size=1200x600");

					driver = new ChromeDriver(options);
				}
				break;
				
			case ANDROID:

				if (driver == null) {

					try {
						driver = new AndroidDriver<MobileElement>(new URL(UrlProperties.URL_SERVER_APPIUM_LOCAL),
								Properties.CAPABILITIES);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					System.out.println("ANDROID driver started...");

				}
				break;

			case IOS:

				if (driver == null) {

					try {
						driver = new IOSDriver<MobileElement>(new URL(UrlProperties.URL_SERVER_APPIUM_LOCAL),
								Properties.CAPABILITIES);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					System.out.println("IOS driver started...");

				}
				break;
				
			default:
				System.out.println("Opcao nao disponivel...");
				break;
			}
		}

		if (Properties.EXECUTION_TYPE == ExecutionType.SAUCELABS) {

			switch (Properties.SYSTEM_CONFIG) {

			case FIREFOX:
				System.out.println("Select Firefox driver ...");
				break;

			case CHROME:
				System.out.println("Select Chrome driver ...");
				break;
				
			case EDGE:
				System.out.println("Select Edge driver ...");
				break;
				
			case ANDROID:

				if (driver == null) {

					try {
						driver = new AndroidDriver<MobileElement>(new URL(UrlProperties.URL_SAUCE_LABS_MOBILE),
								Properties.CAPABILITIES);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					System.out.println("ANDROID driver started...");

				}
				break;

			case IOS:

				if (driver == null) {

					try {
						driver = new IOSDriver<MobileElement>(new URL(UrlProperties.URL_SAUCE_LABS_MOBILE),
								Properties.CAPABILITIES);
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
					System.out.println("IOS driver started...");

				}
				break;

			default:
				System.out.println("Opcao nao disponivel...");
				break;
			}
			try {
				driver = new RemoteWebDriver(
						new URL(UrlProperties.URL_SAUCE_LABS_WEB),
						Properties.CAPABILITIES);
			} catch (MalformedURLException e) {
				System.err.println("Falha na conexão com a Saucelabs");
				e.printStackTrace();
			}

		}
		driver.manage().window().setSize(new Dimension(900, 1100));
		return driver;

	}

	public static WebDriver getDriver() {
		return threadDriver.get();
	}
	
	public static void quitDriver() throws IOException{
		WebDriver driver = getDriver();
		if(driver != null) {
			driver.quit();
			driver = null;
		}
		if(threadDriver != null) {
			threadDriver.remove();
		}
		AppiumServerCommand.stopServer();
	}

}
