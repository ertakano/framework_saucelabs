# language: pt
Funcionalidade: Como Tutor desejo iniciar meu cadastro no site da ElevenChimps

  ###################################################################################################################################
  @CheckEmail @Post
  Esquema do Cenario: "<Cenario>" [checkByEmail] "<Descricao>"
    Dado que o e-mail nao seja cadastrado
    E informar o e-mail na api
    E informar a url check-email
    Quando submeter a requisicao POST
    Então deve receber o codigo "200"
    E retornar o codigo uuid

    Exemplos: 
      | Cenario | Descricao                                                                |
      | CT01    | Validar CheckEmail API - 200 com E-mail valido que não exista no sistema |

  ###################################################################################################################################
  @CheckEmail @Post
  Esquema do Cenario: "<Cenario>" [checkByEmail] "<Descricao>"
    Dado que o e-mail nao seja cadastrado
    E informar o e-mail invalido na api "<email>"
    E informar a url check-email
    Quando submeter a requisicao POST
    Então deve receber o codigo "400"
    E retornar a mensagem de erro "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                                                | email                   | mensagem                            |
      | CT02    | Validar CheckEmail API - 400 com E-mail com formato inválido             | teste%teste.com         | must be a well-formed email address |
      | CT03    | Validar CheckEmail API - 400 com E-mail com formato caracteres inválidos | Ângela.Caçula@teste.com | não é um endereço de e-mail         |
      | CT04    | Validar CheckEmail API - 400 com campo de E-mail em branco               |                         | must not be blank                   |

  ###################################################################################################################################
  @Exists @Get
  Esquema do Cenario: "<Cenario>" [exists] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar a url exists
    Quando submeter a requisicao GET
    Então deve receber o codigo "200"
    E retornar o codigo uuid gerado no cadastro "<uuid>"

    Exemplos: 
      | Cenario | Descricao                                                       | email                   | uuid                                 |
      | CT05    | Validar Exists API - 200 com E-mail valido existente no sistema | minnie.sawayn@yahoo.com | 0278afec-c7a8-4ddc-b172-4bf84af76a8c |

  ###################################################################################################################################
  @Exists @Get
  Esquema do Cenario: "<Cenario>" [exists] "<Descricao>"
    Dado informar o e-mail inexistente na api "<email>"
    E informar a url exists
    Quando submeter a requisicao GET
    Então deve receber o codigo "404"
    E retornar a mensagem de erro Email Nao Encontrado

    Exemplos: 
      | Cenario | Descricao                                       | email           |
      | CT06    | Validar Exists API - 404 com E-mail inexistente | teste$teste.com |

  ###################################################################################################################################
  @Complete @Get
  Esquema do Cenario: "<Cenario>" [getByUuid] "<Descricao>"
    Dado que informei o codigo UUID "<uuid>"
    E informar a url register complete
    Quando submeter a requisicao GET
    Então deve receber o codigo "200"
    E retornar os registros de humano cadastrados
      | email                     | nome   | status   | statusRegistro     |
      | vickie.donnelly@gmail.com | Vickie | COMPLETE | REGISTER_STEP_DIET |
    E retornar os registros do pet cadastrado
      | idPet        | nomePet | sobrenomePet | sexo |problemaDeSaude | dataNasc | idadeEmMeses | idRaca | raca  | pesoEmGramas | pesoIdealEmGramas | comePorDia | proteina | porte | castrado | tamanhoDaRaca | pesoIdealAdulto |
      | petsobrenome | Pet     | Donnelly     | m    |true            | 10/2010  |           95 |      6 | Akita |         1000 |              1200 |          2 | CHICKEN  | THIN  | true     | G             |           35000 |
    E retornar as fases da vida
      | mesInicial | mesFinal | faseDaVida |
      |         96 |      360 | SENIOR     |
      |         14 |       95 | ADULT      |
      |          0 |       14 | PUPPY      |

    Exemplos: 
      | Cenario | Descricao                                        | uuid                                 |
      | CT07    | Validar RegisterDiet API - 200 com dados validos | 2fd0509d-e625-4650-a908-30ae06393f99 |

  ###################################################################################################################################
  @Register @Get
  Esquema do Cenario: "<Cenario>" [getTutor] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar a url register
    Quando submeter a requisicao GET
    Então deve receber o codigo "200"
    E retornar os registros cadastrados
      | Litzy | Ruecker | 05060070 | 11 | 999999999 | friend | Pet | PARTIAL | REGISTER_STEP_HUMAN |

    Exemplos: 
      | Cenario | Descricao                                  | email                   | uuid                                 |
      | CT08    | Validar Register API - 200 com UUID valido | litzy.ruecker@gmail.com | d1a4001e-f4ee-4747-8593-336e166ae735 |

  ###################################################################################################################################
  @Register @Get
  Esquema do Cenario: "<Cenario>" [getTutor] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID invalido "<uuid>"
    E informar a url register
    Quando submeter a requisicao GET
    Então deve receber o codigo "404"
    E retornar a mensagem de erro UUID Nao Encontrado

    Exemplos: 
      | Cenario | Descricao                                    | email            | uuid                                 |
      | CT09    | Validar Register API - 404 com UUID invalido | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d397 |

  ###################################################################################################################################
  @RegisterDiet @Put
  Esquema do Cenario: "<Cenario>" [updateDiet] "<Descricao>"
    Dado que informei o codigo UUID "<uuid>"
    E informar a url register diet
    Quando submeter a requisicao PUT de dieta
    Então deve receber o codigo "200"
    E retornar os registros de humano cadastrados
      | email                   | nome | sobrenome | cep      | ddd | numero    | tipo      | comoConheceu | status   | statusRegistro     |
      | minnie.sawayn@yahoo.com | Nome | Sobrenome | 05060070 |  11 | 999999999 | CELLPHONE | friend       | COMPLETE | REGISTER_STEP_DIET |
    E retornar os registros do pet cadastrado
      | idPet        | nomePet | sobrenomePet | sexo | dataNasc | idadeEmMeses | idRaca | raca  | pesoEmGramas | pesoIdealEmGramas | comePorDia | proteina | porte | castrado |
      | petsobrenome | Pet     | Sobrenome    | m    | 10/2010  |           95 |      6 | Akita |         1000 |              1200 |          3 | CHICKEN  | THIN  | true     |
    E se tem problema de saude retornar os problemas cadastrados
      | problemaDeSaude | alergia | alergiaAlimentar | cardiologica | diabetes | gastrointestinal | renal | outros | descricao  |
      | true            | allergy | food-allergy     | cardiology   | diabetes | gastrointestinal | renal | other  | cao doente |

    Exemplos: 
      | Cenario | Descricao                                        | uuid                                 |
      | CT10    | Validar RegisterDiet API - 200 com dados validos | 0278afec-c7a8-4ddc-b172-4bf84af76a8c |

  ###################################################################################################################################
  @RegisterDiet @Put
  Esquema do Cenario: "<Cenario>" [updateDiet] "<Descricao>"
    Dado que informei o codigo UUID "<uuid>"
    E informar a url register diet
    Quando submeter a requisicao PUT de dieta
    Então deve receber o codigo "404"
    E retornar a mensagem de erro no json "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                        | uuid                                | mensagem  |
      | CT11    | Validar RegisterDiet API - 200 com uuid invalido | 2da5e450-cea0-4ab2-85f7-13f88e13d39 | Not Found |

  ###################################################################################################################################
  @RegisterHuman @Put
  Esquema do Cenario: "<Cenario>" [updateTutorHuman] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar o Nome "<nome>"
    E informar o Sobrenome "<sobrenome>"
    E informar o CEP "<cep>"
    E informar se o CEP faz parte da regiao de entrega "<entrega>"
    E informar o Numero do Celular "<ddd>", "<numero>" e "<tipo>"
    E informar como Conheceu a eleven chimps "<comoConheceu>"
    E informar os nomes dos pets "<pet>"
    E informar a url human register
    Quando submeter a requisicao PUT de humano
    Então deve receber o codigo "200"
    E retornar os registros cadastrados
      | Nome | Sobrenome | 05060070 | 11 | 999999999 | friend | Pet | COMPLETE | REGISTER_STEP_HUMAN |

    Exemplos: 
      | Cenario | Descricao                                         | email                   | uuid                                 | nome | sobrenome | cep      | entrega | ddd | numero    | tipo      | comoConheceu | pet |
      | CT12    | Validar RegisterHuman API - 200 com dados validos | minnie.sawayn@yahoo.com | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | Nome | Sobrenome | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       | Pet |

  ###################################################################################################################################
  @RegisterHuman @Put
  Esquema do Cenario: "<Cenario>" [updateTutorHuman] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar o Nome "<nome>"
    E informar o Sobrenome "<sobrenome>"
    E informar o CEP "<cep>"
    E informar se o CEP faz parte da regiao de entrega "<entrega>"
    E informar o Numero do Celular "<ddd>", "<numero>" e "<tipo>"
    E informar como Conheceu a eleven chimps "<comoConheceu>"
    E informar os nomes dos pets "<pet>"
    E informar a url human register
    Quando submeter a requisicao PUT de humano
    Então deve receber o codigo "200"
    E retornar os registros cadastrados
      | Nome | Sobrenome | 99999999 | 11 | 999999999 | friend | Pet | PARTIAL | REGISTER_STEP_HUMAN_ADDRESS_NOT_COVERED |

    Exemplos: 
      | Cenario | Descricao                                                            | email            | uuid                                 | nome | sobrenome | cep      | entrega | ddd | numero    | tipo      | comoConheceu | pet |
      | CT13    | Validar RegisterHuman API - 200 com dados validos e cep nao atendido | teste5@teste.com | 13a7326e-d975-4f66-98ab-f902b63092c5 | Nome | Sobrenome | 99999999 | false   |  11 | 999999999 | CELLPHONE | friend       | Pet |

  ###################################################################################################################################
  @RegisterHuman @Put
  Esquema do Cenario: "<Cenario>" [updateTutorHuman] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar o Nome "<nome>"
    E informar o Sobrenome "<sobrenome>"
    E informar o CEP "<cep>"
    E informar se o CEP faz parte da regiao de entrega "<entrega>"
    E informar o Numero do Celular "<ddd>", "<numero>" e "<tipo>"
    E informar como Conheceu a eleven chimps "<comoConheceu>"
    E informar os nomes dos pets "<pet>"
    E informar a url human register
    Quando submeter a requisicao PUT de humano
    Então deve receber o codigo "400"
    E retornar a mensagem de erro no json "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                                            | email            | uuid                                 | nome | sobrenome | cep      | entrega | ddd | numero    | tipo      | comoConheceu | pet | mensagem                                           |
      | CT14    | Validar RegisterHuman API - 400 com nome em branco                   | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 |      | Sobrenome | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       | Pet | tutor.name.notBlank                                |
      | CT15    | Validar RegisterHuman API - 400 com nome menor que 2 caracteres      | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | n    | Sobrenome | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       | Pet | tutor.name.size.min.max                            |
      | CT16    | Validar RegisterHuman API - 400 com sobrenome em branco              | teste2@teste.com | e8b2c3e8-e0fb-40a8-a9d4-e2faab3e49d7 | Nome |           | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       | Pet | tutor.lastName.notBlank                            |
      | CT17    | Validar RegisterHuman API - 400 com sobrenome menor que 2 caracteres | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | s         | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       | Pet | tutor.lastName.size.min.max                        |
      | CT18    | Validar RegisterHuman API - 400 com ddd em branco                    | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome | 05060070 | true    |     | 999999999 | CELLPHONE | friend       | Pet | phone.area.notBlank                                |
      | CT19    | Validar RegisterHuman API - 400 com numero telefone em branco        | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome | 05060070 | true    |  11 |           | CELLPHONE | friend       | Pet | phone.number.notBlank                              |
      | CT20    | Validar RegisterHuman API - 400 com tipo em branco                   | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome | 05060070 | true    |  11 | 999999999 |           | friend       | Pet | JSON parse error: Cannot deserialize value of type |
      | CT21    | Validar RegisterHuman API - 400 com Como Conheceu em branco          | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome | 05060070 | true    |  11 | 999999999 | CELLPHONE |              | Pet | tutor.howToKnowEC.notBlank                         |
      | CT22    | Validar RegisterHuman API - 400 com Pet em branco                    | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome | 05060070 | true    |  11 | 999999999 | CELLPHONE | friend       |     | tutor.pets.notBlank                                |
      | CT23    | Validar RegisterHuman API - 400 com CEP em branco                    | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | Nome | Sobrenome |          | true    |  11 | 999999999 | CELLPHONE | friend       | Pet | tutor.zipCode.notBlank                             |

  ###################################################################################################################################
  @RegisterPet @Put
  Esquema do Cenario: "<Cenario>" [updatePet] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar data nascimento pet "<dataNasc>"
    E informar a raca "<raca>"
    E informar se é castrado "<castrado>"
    E informar o sexo do pet "<sexo>"
    E informar se tem problema de saude "<problemaSaude>"
    E informar a descricao do problema "<descricao>"
    E informar os problemas
      | allergy | food-allergy | cardiology | diabetes | gastrointestinal | renal | other |
    E informar o Nome do Pet "<nome>"
    E informar o ID do pet "<petId>"
    E informar o peso ideal em gramas "<pesoIdeal>"
    E informar o Sobrenome do pet "<sobrenome>"
    E informar quantas vezes come por dia "<qtasVezesPorDia>"
    E informar a proteina preferida "<proteina>"
    E informar o porte do pet "<porte>"
    E informar o peso em gramas "<pesoAtual>"
    E informar a url pet register
    Quando submeter a requisicao PUT de Pet
    Então deve receber o codigo "200"
    E retornar os registros de humano cadastrados
      | nome | cep      | ddd | numero    | tipo      | comoConheceu | status   | statusRegistro    |
      | Nome | 05060070 |  11 | 999999999 | CELLPHONE | friend       | COMPLETE | REGISTER_STEP_PET |
    E retornar os registros do pet cadastrado
      | idadeEmMeses |
      |           95 |

    Exemplos: 
      | Cenario | Descricao                                         | email                   | uuid                                 | dataNasc | raca  | castrado | sexo | problemaSaude | descricao  | petId        | pesoIdeal | sobrenome | qtasVezesPorDia | nome | proteina | porte | pesoAtual |
      | CT24    | Validar RegisterHuman API - 200 com dados validos | minnie.sawayn@yahoo.com | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | 10/2010  | Akita | true     | m    | true          | cao doente | petsobrenome |      1200 | Sobrenome |               3 | Pet  | CHICKEN  | THIN  |      1000 |

  ###################################################################################################################################
  @RegisterPet @Put
  Esquema do Cenario: "<Cenario>" [updatePet] "<Descricao>"
    Dado que o e-mail seja cadastrado "<email>"
    E informar o codigo UUID gerado "<uuid>"
    E informar data nascimento pet "<dataNasc>"
    E informar a raca "<raca>"
    E informar se é castrado "<castrado>"
    E informar o sexo do pet "<sexo>"
    E informar se tem problema de saude "<problemaSaude>"
    E informar a descricao do problema "<descricao>"
    E informar os problemas
      | allergy | food-allergy | cardiology | diabetes | gastrointestinal | renal | other |
    E informar o Nome do Pet "<nome>"
    E informar o ID do pet "<petId>"
    E informar o peso ideal em gramas "<pesoIdeal>"
    E informar o Sobrenome do pet "<sobrenome>"
    E informar quantas vezes come por dia "<qtasVezesPorDia>"
    E informar a proteina preferida "<proteina>"
    E informar o porte do pet "<porte>"
    E informar o peso em gramas "<pesoAtual>"
    E informar a url pet register
    Quando submeter a requisicao PUT de Pet
    Então deve receber o codigo "<codigo>"
    E retornar a mensagem de erro no json "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                                                               | codigo | email            | uuid                                 | dataNasc      | raca        | castrado | sexo | problemaSaude | descricao  | petId        | pesoIdeal | sobrenome | qtasVezesPorDia | nome | proteina | porte  | pesoAtual | mensagem                      |
      | CT25    | Validar RegisterHuman API - 400 com data nascimento em branco                           |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 |               | Akita       | true     | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.birthMonth.notBlank       |
      | CT26    | Validar RegisterHuman API - 400 com data nascimento anterior a 30 anos                  |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | dataAnterior  | Akita       | true     | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.birthMonth.min.max        |
      | CT27    | Validar RegisterHuman API - 400 com data nascimento posterior a data atual              |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | dataPosterior | Akita       | true     | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.birthMonth.min.max        |
      | CT28    | Validar RegisterHuman API - 400 com id da raca em branco                                |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | idRacaVazio | true     | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.breed.id.notBlank         |
      | CT29    | Validar RegisterHuman API - 400 com raca em branco                                      |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       |             | true     | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.breed.name.notBlank       |
      | CT30    | Validar RegisterHuman API - 400 com castrado em branco                                  |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       |          | m    | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 |                               |
      | CT31    | Validar RegisterHuman API - 400 com sexo em branco                                      |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     |      | true          | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.gender.notBlank           |
      | CT32    | Validar RegisterHuman API - 400 com problemas de saude em branco                        |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    |               | cao doente | petsobrenome |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 |                               |
      | CT33    | Validar RegisterHuman API - 400 com id do pet em branco                                 |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente |              |      1000 | Sobrenome |               2 | Pet  | CHICKEN  | NORMAL |       900 | pet.id.notBlank               |
      | CT34    | Validar RegisterHuman API - 400 com peso ideal em branco com porte diferente normal     |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |           | Sobrenome |               2 | Pet  | CHICKEN  | THIN   |       900 | pet.idealWeightInGrams.valid  |
      | CT35    | Validar RegisterHuman API - 400 com peso ideal acima de 30% com porte diferente normal  |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               2 | Pet  | CHICKEN  | FAT    |      1000 |                               |
      | CT36    | Validar RegisterHuman API - 400 com peso ideal abaixo de 30% com porte diferente normal |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |      1400 | Sobrenome |               2 | Pet  | CHICKEN  | THIN   |      1000 |                               |
      | CT37    | Validar RegisterHuman API - 400 com sobrenome em branco                                 |    400 | teste2@teste.com | e8b2c3e8-e0fb-40a8-a9d4-e2faab3e49d7 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 |           |               2 | Pet  | CHICKEN  | FAT    |      1000 |                               |
      | CT38    | Validar RegisterHuman API - 400 com quantas vezes come por dia em branco                |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |                 | Pet  | CHICKEN  | FAT    |      1000 | pet.mealsPerDay.notNull       |
      | CT39    | Validar RegisterHuman API - 400 com quantas vezes come por dia com zero limite minimo   |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               0 | Pet  | CHICKEN  | FAT    |      1000 | pet.mealsPerDay.min           |
      | CT40    | Validar RegisterHuman API - 400 com quantas vezes come por dia com seis limite maximo   |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               6 | Pet  | CHICKEN  | FAT    |      1000 | pet.mealsPerDay.max           |
      | CT41    | Validar RegisterHuman API - 400 com nome em branco                                      |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               3 |      | CHICKEN  | FAT    |      1000 | pet.name.notBlank             |
      | CT42    | Validar RegisterHuman API - 400 com proteina em branco                                  |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               3 | Pet  |          | FAT    |      1000 | pet.preferredProtein.notBlank |
      | CT43    | Validar RegisterHuman API - 400 com porte em branco                                     |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               3 | Pet  | CHICKEN  |        |      1000 | pet.shape.notBlank            |
      | CT44    | Validar RegisterHuman API - 400 com peso atual em branco                                |    400 | teste1@teste.com | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | 09/2017       | Akita       | true     | m    | true          | cao doente | petsobrenome |       600 | Sobrenome |               3 | Pet  | CHICKEN  | FAT    |           | pet.currentWeight.notNull     |

  ###################################################################################################################################
  @UploadPetFile @Post
  Esquema do Cenario: "<Cenario>" [uploadPetFile] "<Descricao>"
    Dado que informe o UUID "<uuid>"
    E que o pet seja cadastrado "<petId>"
    E informar o nome do arquivo "<arquivo>"
    E informar a url upload files
    Quando submeter a requisicao POST de upload
    Então deve receber o codigo "200"
    E retornar os registros do upload com PetId "<petId>" e o nome do arquivo "<arquivo>"

    Exemplos: 
      | Cenario | Descricao                                                        | uuid                                 | petId        | arquivo          |
      | CT45    | Validar RegisterHuman API - 200 com envio de arquivo JPG upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoJPG.jpg   |
      | CT46    | Validar RegisterHuman API - 200 com envio de arquivo PNG upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoPNG.png   |
      | CT47    | Validar RegisterHuman API - 200 com envio de arquivo DOCX upload | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoDOCX.docx |
      | CT48    | Validar RegisterHuman API - 200 com envio de arquivo XLSX upload | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoXLSX.xlsx |
      | CT49    | Validar RegisterHuman API - 200 com envio de arquivo DOC upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoDOC.doc   |
      | CT50    | Validar RegisterHuman API - 200 com envio de arquivo XLS upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoXLS.xls   |
      | CT51    | Validar RegisterHuman API - 200 com envio de arquivo PDF upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | ArquivoPDF.pdf   |

  ###################################################################################################################################
  @UploadPetFile2 @Post
  Esquema do Cenario: "<Cenario>" [uploadPetFile] "<Descricao>"
    Dado que informe o UUID "<uuid>"
    E que o pet seja cadastrado "<petId>"
    E informar o nome do arquivo "<arquivo>"
    E informar a url upload files
    Quando submeter a requisicao POST de upload
    Então deve receber o codigo "<codigo>"
    E retornar a mensagem de erro do upload "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                                    | uuid                                 | petId        | codigo | arquivo               | mensagem  |
      | CT52    | Validar RegisterHuman API - 404 pet ID em branco             | 2da5e450-cea0-4ab2-85f7-13f88e13d396 |              |    404 | ArquivoJPG.jpg        | Not Found |
      | CT53    | Validar RegisterHuman API - 400 arquivo com mais de 10 mb    | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | petsobrenome |    400 | ArquivoPDFCom20MB.pdf |           |
      | CT54    | Validar RegisterHuman API - 400 formato arquivo invalido jar | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | petsobrenome |    400 | ArquivoJAR.jar        |           |
      | CT55    | Validar RegisterHuman API - 400 formato arquivo invalido exe | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | petsobrenome |    400 | ArquivoEXE.exe        |           |

  ###################################################################################################################################
  @UploadPetFile @Delete
  Esquema do Cenario: "<Cenario>" [uploadPetFile] "<Descricao>"
    Dado que informe o UUID "<uuid>"
    E que o pet seja cadastrado "<petId>"
    E informar o nome do arquivo "<arquivo>"
    E informar a url upload files para exclusao "<arquivo>"
    Quando submeter a requisicao DELETE de upload
    Então deve receber o codigo "200"

    Exemplos: 
      | Cenario | Descricao                                                           | uuid                                 | petId        | arquivo                       |
      | CT56    | Validar RegisterHuman API - 200 com exclusao de arquivo JPG upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoJPG.jpg   |
      | CT57    | Validar RegisterHuman API - 200 com exclusao de arquivo PNG upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoPNG.png   |
      | CT58    | Validar RegisterHuman API - 200 com exclusao de arquivo DOCX upload | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoDOCX.docx |
      | CT59    | Validar RegisterHuman API - 200 com exclusao de arquivo XLSX upload | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoXLSX.xlsx |
      | CT60    | Validar RegisterHuman API - 200 com exclusao de arquivo DOC upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoDOC.doc   |
      | CT61    | Validar RegisterHuman API - 200 com exclusao de arquivo XLS upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoXLS.xls   |
      | CT62    | Validar RegisterHuman API - 200 com exclusao de arquivo PDF upload  | 0278afec-c7a8-4ddc-b172-4bf84af76a8c | petsobrenome | petsobrenome-ArquivoPDF.pdf   |

  ###################################################################################################################################
  @UploadPetFile @Delete
  Esquema do Cenario: "<Cenario>" [uploadPetFile] "<Descricao>"
    Dado que informe o UUID "<uuid>"
    E que o pet seja cadastrado "<petId>"
    E informar o nome do arquivo "<arquivo>"
    E informar a url upload files para exclusao "<arquivo>"
    Quando submeter a requisicao DELETE de upload
    Então deve receber o codigo "<codigo>"
    E retornar a mensagem de erro do upload "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                                          | uuid                                 | petId        | arquivo                     | codigo | mensagem  |
      | CT63    | Validar RegisterHuman API - 404 com exclusao com IdPet em branco   | 2da5e450-cea0-4ab2-85f7-13f88e13d396 |              | petsobrenome-ArquivoJPG.jpg |    404 | Not Found |
      | CT64    | Validar RegisterHuman API - 404 com exclusao com UUID em branco    |                                      | petsobrenome | petsobrenome-ArquivoPNG.png |    404 | Not Found |
      | CT65    | Validar RegisterHuman API - 404 com exclusao com IdPet inexistente | 2da5e450-cea0-4ab2-85f7-13f88e13d396 | petsobrenom  | petsobrenome-ArquivoJPG.jpg |    404 | Not Found |
      | CT66    | Validar RegisterHuman API - 404 com exclusao com UUID inexistente  | 2da5e450-cea0-4ab2-85f7-13f88e13d39  | petsobrenome | petsobrenome-ArquivoPNG.png |    404 | Not Found |
