# language: pt
Funcionalidade: Como Administrador ou Gestor do sistema Jungle, quero cadastrar uma Dieta (Produto Fabricado)

  ###################################################################################################################################
  @Products @Post
  Esquema do Cenario: "<Cenario>" [saveProduct] "<Descricao>"
    Dado que informe os dados do produto
      | caloriasPorGrama | categoria | descricao          | id | nome         | porte | carboPrincipal | protPrincipal | sku | descricaoTecnica  | tipoProduto |
      |             1000 | Allergo   | Alimentacao Adulta |  1 | Adulto Carne | THIN  | Batata-Doce    | CHICKEN       |   1 | Descricao Tecnica | DIET        |
    E informe os paises disponiveis
      | paises |
      | BR     |
    E informe os atributos
      | id | nome | valor |
      |  1 | img  | jpg   |
    E informe os enriquecimentos
      | Vitamina A | Fibras |
    E informe os ingredientes
      | descricao | pesoEmGramas |
      | Carne     |         1000 |
    E informe os niveis de garantia
      | descricao  | porcentagem |
      | garantia 1 |          10 |
    E informe a fase da vida
      | ADULT |
    E informe o preco
      | listaPrecoEmCentavos | precoVendaEmCentavos |
      |                 1000 |                 2000 |
    E informe as principais caracteristicas
      | alimentacao leve |
    E informar a url de produtos "<url>"
    Quando submeter a requisicao POST de produto
    Então deve receber o codigo da requisicao "<codigo>"

    Exemplos: 
      | Cenario | Descricao                                                 | codigo | url      |
      | CT01    | Validar o cadastro do produto API - 200 com dados validos |    200 | products |

  ###################################################################################################################################
  @Products @Post
  Esquema do Cenario: "<Cenario>" [saveProduct] "<Descricao>"
    Dado que informe os dados do produto
      | caloriasPorGrama | categoria | descricao          | id | nome         | porte | carboPrincipal | protPrincipal | sku | descricaoTecnica  | tipoProduto |
      |             1000 | Allergo   | Alimentacao Adulta |  1 | Adulto Carne | THIN  | Batata-Doce    | CHICKEN       |   1 | Descricao Tecnica | DIET        |
    E informe os paises disponiveis
      | paises |
      | BR     |
    E informe os atributos
      | id | nome | valor |
      |  1 | img  | jpg   |
    E informe os enriquecimentos
      | Vitamina A | Fibras |
    E informe os ingredientes
      | descricao | pesoEmGramas |
      | Carne     |         1000 |
    E informe os niveis de garantia
      | descricao  | porcentagem |
      | garantia 1 |          10 |
    E informe a fase da vida
      | ADULT |
    E informe o preco
      | listaPrecoEmCentavos | precoVendaEmCentavos |
      |                 1000 |                 2000 |
    E informe as principais caracteristicas
      | alimentacao leve |
    E informar a url de produtos "<url>"
    Quando submeter a requisicao GET de produto
    Então deve receber o json de retorno
    E validar os dados informados do produto

    Exemplos: 
      | Cenario | Descricao                                                 | codigo | url      |
      | CT01    | Validar o cadastro do produto API - 200 com dados validos |    200 | products |
