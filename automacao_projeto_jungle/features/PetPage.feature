# language: pt
Funcionalidade: Como Tutor desejo realizar meu cadastro no site da ElevenChimps

  ###################################################################################################################################
  @PetPage
  Esquema do Cenario: "<Cenario>" [Step Pet] "<Descricao>"
    Dado que o usuário tenha cadastrado um pet na pagina de humano "<email>"
    E que o usuário esta na pagina do Pet
    E informou sexo como "<sexo>"
    E informou quando nasceu "<dataNasc>"
    E informou raça "<raca>"
    E informou quilo "<quilo>"
    E informou gramas "<grama>"
    E informou quantas vezes come por dia "<comePorDia>"
    E informou porte "<porte>"
    E informou quilo ideal "<quiloIdeal>"
    E informou gramas ideal "<gramaIdeal>"
    E informou a proteina preferida "<proteina>"
    E informou que é castrado "<castrado>"
    E informou que tem problema de saúde "<temProblema>"
    E informou os problemas
      | Alergia | Alergia alimentar | Cardiologico | Diabetes | Gastrointestinal | Renal | Outro |
    E informou a descrição do problema "<descricao>"
    E anexou um arquivo "<arquivo>"
    Quando submeter as informações
    Então devo visualizar as frases no gênero masculino
    E visualizar o resumo dos dados informados
    E realizar o cadastro com sucesso

    Exemplos: 
      | Cenario | Descricao                                   | email            | sexo  | dataNasc | raca                     | quilo | grama | comePorDia | porte | quiloIdeal | gramaIdeal | proteina | castrado | temProblema | descricao | arquivo |
      | CT01    | Validar cadastro com todos os dados válidos | teste1@teste.com | Macho | 08/2011  | Spitz Alemão             |     0 |   100 |          1 | Magro |          1 |        100 | Carne    | sim      | sim         | alergia   |         |
      #| CT02    | Validar cadastro com todos os dados válidos | teste1@teste.com | Femea | 09/2018  | Cão de D' Água Português |    99 |   900 |          2 | Magra |         99 |      0,900 | Frango   | sim      | sim         | alergia   |         |

  ###################################################################################################################################
  #@PetPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário tenha cadastrado cinco pets na pagina de humano
    E que o usuário esta na pagina do Pet
    E informou os sexos
      | Fêmea | Macho | Fêmea | Macho | Fêmea |
    E informou quando nasceram
      | 01/2010 | 01/1998 | 01/2000 | 12/2017 | mês atual/ano atual |
    E informou raças
      | Dogue Alemão | Vira-Lata | Dálmata | Maltês | Pastor Branco Suiço |
    E informou quilos
      | 99 | 06 | 15 | 0 | 20 |
    E informou gramas
      | 0,500 | 0 | 0,400 | 0,500 | 0,600 |
    E informou quantas vezes come por dia
      | 5 | 2 | 3 | 4 | 1 |
    E informou portes
      | Plus Size | Normal | Gordinha | Magro | Gordinha |
    E informou quilos atuais
      | 10 | - | 13 | 3 | 19 |
    E informou gramas
      | 0,900 | - | 0 | 0 | 0 |
    E informou que é castrado
      | não | sim | não | sim | não |
    E informou que tem problema de saúde
      | não | sim | sim | sim | sim |
    E informou que tem os problemas
      | - | Alergia Alimentar | Cardialógico, Diabetes | Gastrointestinal, Renal | Outro |
    E informou a descrição do problema
      | Texto |
    E anexou arquivos
      | - | DOC, DOCX | JPG, JPEG | PNG | XLS |
    Quando submeter as informações
    Então visualizar o resumo dos dados informados
    E realizar os  cadastros com sucesso

    Exemplos: 
      | Cenario | Descricao                    |
      | CT03    | Validar campo Nome em branco |

  ###################################################################################################################################
  #@PetPage
  Esquema do Cenario: "<Cenario>" [Step Pet] "<Descricao>"
    Dado que o usuário tenha cadastrado um pet na pagina de humano "<email>"
    E que o usuário esta na pagina do Pet
    E informou sexo como "<sexo>"
    E informou quando nasceu "<dataNasc>"
    E informou raça "<raca>"
    E informou quilo "<quilo>"
    E informou gramas "<grama>"
    E informou quantas vezes come por dia "<comePorDia>"
    E informou porte "<porte>"
    E informou quilo ideal "<quiloIdeal>"
    E informou gramas ideal "<gramaIdeal>"
    E informou que é castrado "<castrado>"
    E informou que tem problema de saúde "<temProblema>"
    E informou os problemas
      | alergia | outros |
    E informou a descrição do problema "<descricao>"
    E anexou um arquivo "<arquivo>"
    Quando submeter as informações
    Então devo visualizar as frases no gênero masculino
    E visualizar o resumo dos dados informados
    E realizar o cadastro com sucesso

    Exemplos: 
      | Cenario | Descricao                                                                | sexo | dataNasc | raca                     | quilo | grama | comePorDia | porte  | quiloIdeal | gramaIdeal | castrado | temProblema | descricao | arquivo |
      | CT04    | Validar raca inexistente                                                 | m    | 10/2010  | Akita                    |   100 |   100 |          1 | NORMAL |        100 |        100 | sim      | sim         | alergia   |         |
      | CT05    | Validar informou raca em branco                                          | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT06    | Validar data nascimento com data futura                                  | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT07    | Validar data nascimento com data anterior a 30 anos                      | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT08    | Validar data nascimento em branco                                        | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT09    | Validar quilo nao informado                                              | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT10    | Validar grama nao informado                                              | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT11    | Validar porte nao informado                                              | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT12    | Validar se é castrado nao informado                                      | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT13    | Validar se tem problemas de saude nao informado                          | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT14    | Validar porte físico diferente de Normal e nao informou quilo ideal      | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT15    | Validar porte físico diferente de Normal e nao informou grama ideal      | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT16    | Validar Peso Ideal 30% abaixo do Peso informado em Quanto Pesa           | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT17    | Validar Peso Ideal 30% acima do Peso informado em Quanto Pesa            | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |
      | CT18    | Validar que informou com problema e não informou a descrição do problema | f    | 09/2018  | Cão de D' Água Português |    99 | 0,900 |          2 | MAGRA  |         99 |      0,900 | sim      | sim         | alergia   |         |

  ###################################################################################################################################
  #@PetPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário tenha cadastrado um pet na pagina de humano "<email>"
    E que o usuário esta na pagina do Pet
    E informou com problema de saúde
    E não selecionou os problemas listados
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@PetPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário tenha cadastrado um pet na pagina de humano "<email>"
    E que o usuário esta na pagina do Pet
    E informou todos os dados obrigatórios
    E anexou um arquivo
    Quando excluir o arquivo
    Então o arquivo deve ser removido do cadastro

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT20    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |
