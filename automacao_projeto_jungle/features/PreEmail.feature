# language: pt
Funcionalidade: Como Tutor desejo iniciar meu cadastro no site da ElevenChimps

  ###################################################################################################################################
  @PreEmail 
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário não possua um e-mail cadastrado no sistema
    E digitar o e-mail "<email>"
    Quando submeter os dados
    Então deve visualizar a página para cadastrar os dados de Tutor

    Exemplos: 
      | Cenario | Descricao                                | email |
      | CT01    | Validar que E-mail não existe no sistema |       |

  ###################################################################################################################################
  @PreEmail
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário possua um e-mail com cadastro incompleto no sistema
    E digitar o e-mail "<email>"
    Quando submeter os dados
    Então deve visualizar a página de Tutor com os dados previamente preenchidos
    E visualizar o Nome "<nome>"
    E visualizar o Sobrenome "<sobrenome>"
    E visualizar o E-mail "<email>"
    E visualizar o CEP "<cep>"
    E visualizar o Celular "<celular>"
    E visualizar Como Conheceu a Eleven Chimps "<comoConheceu>"
    E visualizar Quantos Cachorros Tem "<qtosPets>"
    E visualizar os Nomes dos Cachorros "<dogUm>", "<dogDois>", "<dogTres>", "<dogQuatro>", "<dogCinco>"

    Exemplos: 
      | Cenario | Descricao                                         | email           | nome           | sobrenome      | cep       | celular         | comoConheceu | dogCinco | dogQuatro | dogTres | dogDois | dogUm | qtosPets |
      | CT02    | Validar que E-mail existe com cadastro incompleto | teste@teste.com | Paulo de Abreu | Vicente Júnior | 05060-030 | (11) 99999-9999 | Facebook     | dogcinco | dogquatro | dogtres | dogdois | dogum |        5 |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário possua um e-mail cadastrado
    E tenha realizado o cadastro de Tutor
    E digitar o e-mail "<email>"
    Quando submeter os dados
    Então deve visualizar o campo de senha
    E o link esqueci minha senha

    Exemplos: 
      | Cenario | Descricao                                                | email           |
      | CT03    | Validar Tela mostrará campo de senha para ser preenchida | teste@teste.com |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail cadastrado "<email>"
    E insira uma senha cadastrada "<senha>"
    Quando submeter os dados
    Então deve visualizar a página de FAQ

    Exemplos: 
      | Cenario | Descricao                                                | email           | senha  | site                                   |
      | CT04    | Validar senha correta e cliente é autenticado no sistema | teste@teste.com | 123456 | https://suporte.elevenchimps.com.br/hc |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail cadastrado "<email>"
    Quando clicar em esqueci minha senha
    Então deve receber um e-mail para criar nova senha
    E login deve ser bloqueado até a troca da senha

    Exemplos: 
      | Cenario | Descricao                               | email           |
      | CT05    | Validar recuperação de senha por e-mail | teste@teste.com |

  ###################################################################################################################################
  @PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail inválido
    E digitar o e-mail "<email>"
    Quando submeter os dados
    Então nao devo prosseguir para o proximo passo

    Exemplos: 
      | Cenario | Descricao                                       | email                   |
      | CT06    | Validar E-mail com formato inválido             | teste#teste.com         |
      | CT07    | Validar E-mail com formato caracteres inválidos | Ângela.Caçula@teste.com |

  ###################################################################################################################################
  @PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail em branco
    Quando submeter os dados
    Então nao devo prosseguir para o proximo passo

    Exemplos: 
      | Cenario | Descricao                         |
      | CT08    | Validar campo de E-mail em branco |

  ###################################################################################################################################
  @PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E digitar o e-mail "<email>"
    E que o usuário insira um e-mail em branco
    Quando submeter os dados
    Então deve visualizar uma mensagem de erro para o campo E-mail "<mensagem>"
    E nao devo prosseguir para o proximo passo

    Exemplos: 
      | Cenario | Descricao                                              | email           | mensagem              |
      | CT09    | Validar mensagem de campo E-mail em branco obrigatorio | teste@teste.com | E-mail é obrigatório. |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail cadastrado "<email>"
    Quando submeter a senha inválida "<senha>"
    Então deve visualizar a mensagem "<mensagem>"

    Exemplos: 
      | Cenario | Descricao              | email           | senha  | mensagem |
      | CT10    | Validar senha inválido | teste@teste.com | 000000 | mensagem |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail cadastrado "<email>"
    Quando submeter a senha em branco "<senha>"
    Então deve visualizar a mensagem "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                   | email           | senha | mensagem |
      | CT11    | Validar Senha não informada | teste@teste.com |       | mensagem |

  ###################################################################################################################################
  #@PreEmail
  Esquema do Cenario: "<Cenario>" [Pré E-mail] "<Descricao>"
    Dado que eu esteja na home pre-email
    E que o usuário insira um e-mail cadastrado "<email>"
    Quando submeter a senha com token expirado "<senha>"
    Então deve visualizar a mensagem "<mensagem>"

    Exemplos: 
      | Cenario | Descricao                                    | email           | senha  | mensagem |
      | CT12    | Validar Esqueci a senha com o token expirado | teste@teste.com | 111111 | mensagem |
