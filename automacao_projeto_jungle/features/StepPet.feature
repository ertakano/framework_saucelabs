# language: pt
Funcionalidade: Como Tutor desejo realizar meu cadastro no site da ElevenChimps

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou sexo como Macho
    E informou quando nasceu 08/2011
    E informou raça como Spitz Alemão
    E informou quilo como zero
    E informou gramas com 0,100
    E informou quantas vezes come por dia como 1
    E informou porte como Magro
    E informou quilo com zero
    E informou gramas com 0,100
    E informou que é castrado
    E informou que tem problema de saúde
    E informou que tem alergia e outro problema
    E informou a descrição do problema
    E anexou um arquivo PDF
    Quando submeter as informações
    Então devo visualizar as frases no gênero masculino
    E visualizar o resumo dos dados informados
    E realizar o cadastro com sucesso

    Exemplos: 
      | Cenario | Descricao                                   | nome                                | sobrenome                           | cep       | ddd | numero    | comoConheceu |
      | CT01    | Validar cadastro com todos os dados válidos | Ângela Caçula Júnior Ísis Miró João | Ângela Caçula Júnior Ísis Miró João | 05060-030 |  11 | 999999999 | friend       |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou sexo como Fêmea
    E informou quando nasceu 08/2018
    E informou raça como Cão de D' Água Português
    E informou quilo como 99
    E informou gramas com 0,900
    E informou quantas vezes come por dia como 2
    E informou porte como Magra
    E informou quilo com 99
    E informou gramas com 0,900
    E informou que é castrada
    E informou que tem problema de saúde
    E informou que tem outro problema
    E informou a descrição do problema
    E anexou um arquivo PDF
    Quando sair do sistema
    E realizar o login novamente
    Então devo visualizar as frases no gênero feminino
    E visualizar o resumo dos dados informados
    E visualizar os dados cadastrados previamente com sucesso

    Exemplos: 
      | Cenario | Descricao                                          | nome                                | sobrenome                           | cep       | ddd | numero    | comoConheceu | qtosDogs |
      | CT02    | Validar cadastro com os dados obrigatórios válidos | Ângela Caçula Júnior Ísis Miró João | Ângela Caçula Júnior Ísis Miró João | 05060-030 |  11 | 999999999 | friend       |        1 |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou os sexos
      | Fêmea | Macho | Fêmea | Macho | Fêmea |
    E informou quando nasceram
      | 01/2010 | 01/1998 | 01/2000 | 12/2017 | mês atual/ano atual |
    E informou raças
      | Dogue Alemão | Vira-Lata | Dálmata | Maltês | Pastor Branco Suiço |
    E informou quilos
      | 99 | 06 | 15 | 0 | 20 |
    E informou gramas
      | 0,500 | 0 | 0,400 | 0,500 | 0,600 |
    E informou quantas vezes come por dia
      | 5 | 2 | 3 | 4 | 1 |
    E informou portes
      | Plus Size | Normal | Gordinha | Magro | Gordinha |
    E informou quilos atuais
      | 10 | - | 13 | 3 | 19 |
    E informou gramas
      | 0,900 | - | 0 | 0 | 0 |
    E informou que é castrado
      | não | sim | não | sim | não |
    E informou que tem problema de saúde
      | não | sim | sim | sim | sim |
    E informou que tem os problemas
      | - | Alergia Alimentar | Cardialógico, Diabetes | Gastrointestinal, Renal | Outro |
    E informou a descrição do problema
      | Texto |
    E anexou arquivos
      | - | DOC, DOCX | JPG, JPEG | PNG | XLS |
    Quando submeter as informações
    Então visualizar o resumo dos dados informados
    E realizar os  cadastros com sucesso

    Exemplos: 
      | Cenario | Descricao                                   | nome                    | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem            |
      | CT03    | Validar campo Nome em branco                |                         | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Nome é obrigatório. |
      | CT04    | Validar campo Nome com caracteres inválidos | '@#+-/*%!-_;?=.,()&$<>' | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.    |
      | CT05    | Validar campo Nome com números              |              0123456789 | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.    |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou uma raça inexistente
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de registro não encontrado

    Exemplos: 
      | Cenario | Descricao                                        | nome      | sobrenome               | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem                 |
      | CT06    | Validar campo Sobrenome em branco                | Miró João |                         | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Sobrenome é obrigatório. |
      | CT07    | Validar campo Sobrenome com caracteres inválidos | Miró João | '@#+-/*%!-_;?=.,()&$<>' | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.         |
      | CT08    | Validar campo Sobrenome com números              | Miró João |              0123456789 | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.         |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou uma raça
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                              | nome          | sobrenome | cep                      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem           |
      | CT09    | Validar campo CEP em branco            | Ângela Caçula | Miró João |                          |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Cep é obrigatório. |
      | CT10    | Validar campo CEP diferente de números | Ângela Caçula | Miró João | 'a@#+-/*%!-_;?=.,()&$<>' |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Cep é obrigatório. |

  #| CT11    | Validar campo CEP inexistente          | Ângela Caçula | Miró João |                 99999999 | 11999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.   |
  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou nascimento com data futura
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de data inválida

    Exemplos: 
      | Cenario | Descricao                                    | nome          | sobrenome | cep      | ddd                      | numero                   | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem               |
      | CT12    | Validar campo Celular em branco              | Ângela Caçula | Miró João | 05060030 |                          |                          | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. |
      | CT13    | Validar campo Celular diferente de números   | Ângela Caçula | Miró João | 05060030 | 'a@#+-/*%!-_;?=.,()&$<>' | 'a@#+-/*%!-_;?=.,()&$<>' | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. |
      | CT14    | Validar campo Celular com menos de 8 dígitos | Ângela Caçula | Miró João | 05060030 |                       11 |                  1234567 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou nascimento com data anterior a 30 anos
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de data inválida

    Exemplos: 
      | Cenario | Descricao                                            | nome          | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem                         |
      | CT15    | Validar campo Como conheceu a ElevenChimps em branco | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 |              | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Como nos conheceu é obrigatório. |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou a data de nascimento
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                               | nome          | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm                   | dogDois                 | dogTres                 | dogQuatro               | dogCinco                | mensagem            |
      | CT16    | Validar campo Nome do Cachorro em branco                | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       |                         |                         |                         |                         |                         | Nome é obrigatório. |
      | CT17    | Validar campo Nome do Cachorro com caracteres inválidos | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | Dados inválidos.    |
      | CT18    | Validar campo Nome do Cachorro com números              | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       |              0123456789 |              0123456789 |              0123456789 |              0123456789 |              0123456789 | Dados inválidos.    |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou o quilo
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou as gramas
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou o porte físico
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou se é castrado
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E não informou se tem problema de saúde
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou com problema de saúde
    E não selecionou os problemas listados
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou o porte físico diferente de Normal
    E não informou o quilo ideal
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou o porte físico diferente de Normal
    E não informou a grama ideal
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou o porte físico diferente de Normal
    E informou o Peso Ideal 30% abaixo do Peso informado em Quanto Pesa
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou o porte físico diferente de Normal
    E informou o Peso Ideal 30% acima do Peso informado em Quanto Pesa
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou com problema de saúde
    E não informou a descrição do problema
    E informou todos os outros dados obrigatórios
    Quando submeter as informações
    Então não deve registrar as informações
    E deve exibir uma mensagem de campo obrigatório

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que o usuário logou no sistema
    E informou todos os dados obrigatórios
    E anexou um arquivo
    Quando excluir o arquivo
    Então o arquivo deve ser removido do cadastro

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |
