# language: pt
Funcionalidade: Como Tutor desejo realizar meu cadastro no site da ElevenChimps

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E que informou o campo Nome com letras,  acento agudo, circunflexo, til e cedilha "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informar os Nomes dos cachorros válidos
      | Dog 5 | Dog 4 | Dog 3 | Dog 2 | Dog 1 |
    Quando submeter as informações
    Então deve visualizar o cadastro de cachorros

    Exemplos: 
      | Cenario | Descricao                                   | nome                                | sobrenome                           | cep       | ddd | numero    | comoConheceu |
      | CT01    | Validar cadastro com todos os dados válidos | Ângela Caçula Júnior Ísis Miró João | Ângela Caçula Júnior Ísis Miró João | 05060-030 |  11 | 999999999 | friend       |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E que informou o campo Nome com letras,  acento agudo, circunflexo, til e cedilha "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "<qtosDogs>"
    E informar os Nomes dos cachorros válidos
      | Ângela Caçula Júnior Ísis Miró João |
    Quando submeter as informações
    Então deve visualizar o cadastro de cachorros

    Exemplos: 
      | Cenario | Descricao                                          | nome                                | sobrenome                           | cep       | ddd | numero    | comoConheceu | qtosDogs |
      | CT02    | Validar cadastro com os dados obrigatórios válidos | Ângela Caçula Júnior Ísis Miró João | Ângela Caçula Júnior Ísis Miró João | 05060-030 |  11 | 999999999 | friend       |        1 |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo Nome "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                                   | nome                    | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem            |
      | CT03    | Validar campo Nome em branco                |                         | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Nome é obrigatório. |
      | CT04    | Validar campo Nome com caracteres inválidos | '@#+-/*%!-_;?=.,()&$<>' | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.    |
      | CT05    | Validar campo Nome com números              |              0123456789 | Miró João | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.    |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo Sobrenome "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                                        | nome      | sobrenome               | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem                 |
      | CT06    | Validar campo Sobrenome em branco                | Miró João |                         | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Sobrenome é obrigatório. |
      | CT07    | Validar campo Sobrenome com caracteres inválidos | Miró João | '@#+-/*%!-_;?=.,()&$<>' | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.         |
      | CT08    | Validar campo Sobrenome com números              | Miró João |              0123456789 | 05060030 |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.         |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo CEP "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                              | nome          | sobrenome | cep                      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem           |
      | CT09    | Validar campo CEP em branco            | Ângela Caçula | Miró João |                          |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Cep é obrigatório. |
      | CT10    | Validar campo CEP diferente de números | Ângela Caçula | Miró João | 'a@#+-/*%!-_;?=.,()&$<>' |  11 | 999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Cep é obrigatório. |

  #| CT11    | Validar campo CEP inexistente          | Ângela Caçula | Miró João |                 99999999 | 11999999999 | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Dados inválidos.   |
  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo Celular "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                                    | nome          | sobrenome | cep      | ddd                      | numero                   | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem               | 
      | CT12    | Validar campo Celular em branco              | Ângela Caçula | Miró João | 05060030 |                          |                          | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. | 
      | CT13    | Validar campo Celular diferente de números   | Ângela Caçula | Miró João | 05060030 | 'a@#+-/*%!-_;?=.,()&$<>' | 'a@#+-/*%!-_;?=.,()&$<>' | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. | 
      | CT14    | Validar campo Celular com menos de 8 dígitos | Ângela Caçula | Miró João | 05060030 |                       11 |                 1234567  | friend       | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Celular é obrigatório. | 

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo Como conheceu "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                                            | nome          | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm | dogDois | dogTres | dogQuatro | dogCinco | mensagem                         |
      | CT15    | Validar campo Como conheceu a ElevenChimps em branco | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 |              | dogUm | dogDois | dogTres | dogQuatro | dogCinco | Como nos conheceu é obrigatório. |

  ###################################################################################################################################
  @HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E informou o campo Nome "<nome>"
    E informou Sobrenome "<sobrenome>"
    E informou CEP "<cep>"
    E informou Celular com DDD "<ddd>" e Numero "<numero>"
    E informou Como conheceu a ElevenChimps "<comoConheceu>"
    E informou Quantos cachorros tem com o valor de "5"
    E informou o nome dos cachorros petUm "<dogUm>",petDois "<dogDois>",petTres "<dogTres>",peQuatro "<dogQuatro>",petCinco "<dogCinco>"
    Quando submeter as informações invalidas
    Então deve visualizar uma mensagem de erro para o campo Nome do Cachorro "<mensagem>"
    E não deve prosseguir com o cadastro

    Exemplos: 
      | Cenario | Descricao                                               | nome          | sobrenome | cep      | ddd | numero    | comoConheceu | dogUm                   | dogDois                 | dogTres                 | dogQuatro               | dogCinco                | mensagem            |
      | CT16    | Validar campo Nome do Cachorro em branco                | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       |                         |                         |                         |                         |                         | Nome é obrigatório. |
      | CT17    | Validar campo Nome do Cachorro com caracteres inválidos | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | '@#+-/*%!-_;?=.,()&$<>' | Dados inválidos.    |
      | CT18    | Validar campo Nome do Cachorro com números              | Ângela Caçula | Miró João | 05060030 |  11 | 999999999 | friend       |              0123456789 |              0123456789 |              0123456789 |              0123456789 |              0123456789 | Dados inválidos.    |

  ###################################################################################################################################
  #@HumanPage
  Esquema do Cenario: "<Cenario>" [Step Tutor] "<Descricao>"
    Dado que eu esteja na home do humano
    E que o usuário logou no sistema com "" e "123456"
    E que informou CEP que não faz parte da região da entrega "<cep>"
    E informou outros dados obrigatórios "<nome>","<sobrenome>","<ddd>","<numero>","<comoConheceu>","<qtosDogs>","<nomeDog1>"
    Quando submeter as informações
    Então deve visualizar as informações que a região não faz parte da entrega
    E visualizar o cupom de desconto

    Exemplos: 
      | Cenario | Descricao                                             | nome   | sobrenome | cep       | ddd | numero    | comoConheceu | qtosDogs | nomeDog1 |
      | CT19    | Validar quando CEP não faz parte da região da entrega | Ângela | Miró João | 05060-030 |  11 | 999999999 | Amigos       |        1 | Dog1     |
